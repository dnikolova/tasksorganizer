//
//  TaskStatusViewController.m
//  Tasks
//
//  Created by Desislava Nikolova on 12/11/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import "TaskStatusViewController.h"

@interface TaskStatusViewController ()

@end

@implementation TaskStatusViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _done.hidden = YES;
	// Do any additional setup after loading the view.
    [completedBtn setImage:[UIImage imageNamed:@"checkmarkSelected.png"] forState:UIControlStateSelected];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    taskName.text = [AppData instance].workingTask.title;
    taskDesc.text = [AppData instance].workingTask.description;
    
    taskPriority.selectedSegmentIndex = [AppData instance].workingTask.taskPriority - 1;
    NSLog(@"task completed ? %d",[AppData instance].workingTask.status);
    completedBtn.selected = [AppData instance].workingTask.status;
    
    [self didChangePriority:nil];
}

-(IBAction)completeTaskBtnAction:(id)sender
{
    completedBtn.selected = ! completedBtn.selected;
    [AppData instance].workingTask.status = (completedBtn.selected) ? 1 : 0;
    
    NSLog(@"task completed %d",[AppData instance].workingTask.status);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)saveNewTaskBtnAction
{
    [AppData instance].workingTask.title = taskName.text;
    [AppData instance].workingTask.description = taskDesc.text;
    [AppData instance].workingTask.taskPriority = 1;
    if(taskPriority.selectedSegmentIndex == 1)
        [AppData instance].workingTask.taskPriority = 2;
    if(taskPriority.selectedSegmentIndex == 2)
        [AppData instance].workingTask.taskPriority = 3;
    [AppData instance].workingTask.status = completedBtn.selected;//pending
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveEditTask" object:nil];
}


-(IBAction)didChangePriority:(id)sender
{
    if(taskPriority.selectedSegmentIndex == 0)
        _imgPriority.image = [UIImage imageNamed:@"redBall"];
    if(taskPriority.selectedSegmentIndex == 1)
        _imgPriority.image = [UIImage imageNamed:@"yellowBall"];
    if(taskPriority.selectedSegmentIndex == 2)
        _imgPriority.image = [UIImage imageNamed:@"greenBall"];
}

-(IBAction)cancelBtnAction
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CancelEditTask" object:nil];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    _done.hidden = NO;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    _done.hidden = NO;
}

-(IBAction)done
{
    [taskDesc resignFirstResponder];
    [taskName resignFirstResponder];
    _done.hidden = YES;
}

@end
