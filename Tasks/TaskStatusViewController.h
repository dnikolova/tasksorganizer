//
//  TaskStatusViewController.h
//  Tasks
//
//  Created by Desislava Nikolova on 12/11/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppData.h"

@interface TaskStatusViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate>
{
    IBOutlet UITextField *taskName;
    IBOutlet UITextView *taskDesc;
    IBOutlet UISegmentedControl *taskPriority;
    IBOutlet UIButton *completedBtn;
    IBOutlet UIButton *_done;
    IBOutlet UIImageView *_imgPriority;
}

-(IBAction)didChangePriority:(id)sender;

@end
