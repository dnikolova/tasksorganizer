//
//  WeatherInfoView.h
//  Tasks
//
//  Created by Desislava Nikolova on 1/12/13.
//  Copyright (c) 2013 DesiNikolova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherInfoView : UIView
{
    IBOutlet UIImageView *_weatherIcon1,*_weatherIcon2;
    IBOutlet UILabel *_degrees, *_condistions, *_locationAndTime;
    IBOutlet UILabel *_dateLabel;
    IBOutlet UIButton *_setLoc, *_more;
    
    IBOutlet UIView *_wInfoView;
    IBOutlet UIView *_locInfoView;
    bool isInfoRotating;
    bool displayingWeatherInfo;
    
    NSTimer *_timer;
    
    IBOutlet UILabel *_noData;
}

-(IBAction)weatherMore:(id)sender;
-(IBAction)reloadWeather:(id)sender;
-(void)startRotatingInfo;
-(void)updateWeather;

+(void)transitionDidStop:(NSString *)animationID finished:(BOOL)finished context:(void *)context;

@end
