//
//  NewTaskViewController.h
//  Tasks
//
//  Created by Desislava Nikolova on 11/25/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "Constants.h"

@class ViewController;

@interface NewTaskViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
    IBOutlet UIButton *_done;
    IBOutlet UIPickerView *_pickerView, *_topicPickerView;
    float pickerYPos;
    bool editStartTime;
    NSString *newHour, *newMins;
    
    NSMutableArray *topicsArray, *topicsNames;
    IBOutlet UIImageView *topicImg, *priorityDot;
    IBOutlet UILabel *topicLabel;
    bool loadingTopics;
}

@property(nonatomic,retain)UIViewController *delegate;
@property(nonatomic, retain)IBOutlet UITextField *taskName;
@property(nonatomic, retain)IBOutlet UITextView *taskDesc;
@property(nonatomic, retain)IBOutlet UITextField *startTime, *endTime;
@property(nonatomic, retain)IBOutlet UISegmentedControl *taskPriority;

-(IBAction)saveNewTaskBtnAction;
-(IBAction)cancelBtnAction;
-(IBAction)didChangePriority:(id)sender;

@end
