//
//  HoursGridView.h
//  Tasks
//
//  Created by Desislava  Nikolova on 9/16/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HourLabel;

@interface HoursGridView : UIView
{
    HourLabel *_task;
}

-(void)updateBtn;
-(void)updateGridView;
-(void)scrollToTop;

@end
