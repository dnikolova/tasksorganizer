//
//  CalenderSelectorView.m
//  Tasks
//
//  Created by Desislava Nikolova on 3/31/13.
//  Copyright (c) 2013 DesiNikolova. All rights reserved.
//

#import "CalenderSelectorView.h"
#import "AppData.h"

@implementation CalenderSelectorView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        nextD.text = [NSString stringWithFormat:@"%d",[[AppData instance] currentDay]];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"ShowCalenderSelection" object:nil];
    //[self verticalFlip];
}

- (void)verticalFlipNext:(BOOL)isNext dateNum:(int)dateNum{
    
    if(isNext)
    {
    
        [UIView animateWithDuration:0.55
                     animations:^{ nextPage.layer.transform = CATransform3DMakeRotation(M_PI_2,1.0,0.0,0.0);}
                     completion:^(BOOL finished){
                          }];
    }else{
        [UIView animateWithDuration:0.55
                         animations:^{ nextPage.layer.transform = CATransform3DMakeRotation(M_PI_2,1.0,0.0,0.0);}
                         completion:^(BOOL finished){
                         }];
    }
}

-(void)goToPage:(int)page flipUp:(BOOL)flipUp {
    
    //do stuff...
    nextPage.hidden = NO;
    // start the animated transition
    [UIView beginAnimations:@"Next" context:nil];
    [UIView setAnimationDuration:0.44];
    [UIView setAnimationTransition:flipUp ? UIViewAnimationTransitionCurlUp : UIViewAnimationTransitionCurlDown forView:nextPage cache:YES];
    
    //insert your new subview
    //[self.view insertSubview:currentPage.view atIndex:self.view.subviews.count];
    
    // commit the transition animation
    nextD.text = [NSString stringWithFormat:@"%d", page];
    [UIView commitAnimations];
}

-(void)transitionDidStop:(NSString *)animationID finished:(BOOL)finished context:(void *)context
{
    
    if([animationID isEqualToString:@"Next"])
    {
        nextPage.hidden = YES;
    }
}

@end
