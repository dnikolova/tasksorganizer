//
//  HelpViewController.h
//  Tasks
//
//  Created by Desislava Nikolova on 4/3/13.
//  Copyright (c) 2013 DesiNikolova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController
{
    IBOutlet UIScrollView *_scrollView;
    IBOutlet UIButton *_prevBtn, *_nextBtn;
    int numImages, currentImage;
    IBOutlet UIImageView *_img;
    IBOutlet UILabel *instructionsView;
    int currentView;
}

-(IBAction)navigation:(id)sender;
-(IBAction)newtask:(id)sender;
-(IBAction)settings:(id)sender;
-(IBAction)cancelBtnAction;

-(IBAction)prevBtnAction;
-(IBAction)nextBtnAction;

@end
