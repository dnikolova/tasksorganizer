//
//  CalenderSelectorView.h
//  Tasks
//
//  Created by Desislava Nikolova on 3/31/13.
//  Copyright (c) 2013 DesiNikolova. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface CalenderSelectorView : UIView
{
    IBOutlet UIView *nextPage, *prevPage;
    bool flipped;
    IBOutlet UILabel *nextD, *prevD;
}

-(void)goToPage:(int)page flipUp:(BOOL)flipUp ;

@end
