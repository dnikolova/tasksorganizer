//
//  DataManager.h
//  Tasks
//
//  Created by Desislava  Nikolova on 9/16/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManager.h>

enum{
    WOEID_OPERATION = 10,
    CONDITIONS_OPERATION
}WeatherOperation;

@interface DataManager : NSObject <NSURLConnectionDelegate, NSXMLParserDelegate, CLLocationManagerDelegate>
{
    NSMutableData *receivedData;
    NSURLConnection *conn;
    NSString *receivedString;
    NSString *currentElementValue;
    
    int weatherOpeartion;
    int woeid;
    
    bool doRefresh;
    
    CLLocationManager *locationManger;
}

@property(nonatomic, assign) float locationLat;
@property(nonatomic, assign) float locationLong;
@property(nonatomic, retain) NSDictionary *loacationInfo;
@property(nonatomic, retain) NSDictionary *weatherInfo;
@property(nonatomic, retain) NSDictionary *windInfo;
@property(nonatomic, retain) NSDictionary *atmosphereInfo;
@property(nonatomic, retain) NSDictionary *astronomyInfo;
@property(nonatomic, retain) NSDictionary *unitsInfo;
@property(nonatomic, assign) bool woeidLoaded;

+(DataManager*)instance;
-(void)getWeatherWoeid;
-(void)loadWeather;
-(NSMutableArray*)weatherIconForCode:(int)code;
-(void)refreshWeather;
-(void)startTrackingWeatherData;

@end
