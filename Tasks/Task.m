//
//  Task.m
//  Tasks
//
//  Created by Desislava  Nikolova on 9/16/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import "Task.h"

@implementation Task

@synthesize view, title, description, cateory, buttonTitle;
@synthesize startHour, startMinutes, endHour, endMinutes;
@synthesize taskID, status, taskPriority;
@synthesize edited,displayed;
@synthesize day, year, month, isAM;

-(id)init
{
    self = [super init];
    
    self.title = @"Task title";
    self.description = @"Task description goes here...description goes here...description goes here...description goes here...description goes here...description goes here...description goes here...";
    self.cateory = 0;
    self.taskPriority = 2;
    self.status = 0;
    //
    self.startHour = 0;
    self.startMinutes = 0;
    self.endHour = 0;
    self.endMinutes = 0;
    //
    self.taskID = 0;
    self.edited = NO;
    
    self.day = -1;
    self.month = -1;
    self.year = -1;
    
    self.displayed = NO;
    
    return self;
}
-(Task*)copyTaskObject
{
    Task *copy = [[Task alloc] init];
    copy.title = [NSString stringWithString:self.title];
    copy.description = [NSString stringWithString:self.description];
    copy.cateory = self.cateory;
    copy.taskPriority = self.taskPriority;
    copy.status = self.status;
    //
    copy.startHour = self.startHour;
    copy.startMinutes = self.startMinutes;
    copy.endHour = self.endHour;
    copy.endMinutes = self.endMinutes;
    //
    copy.taskID = self.taskID;
    copy.day = self.day;
    copy.month = self.month;
    copy.year = self.year;
    copy.displayed = self.displayed;
    
    return copy;
    
}

-(void)reset
{
    self.title = @"Task title";
    self.description = @"Task description goes here...description goes here...description goes here...description goes here...description goes here...description goes here...description goes here...";
    self.cateory = 0;
    self.taskPriority = 2;
    self.status = 0;
    //
    self.endHour = (startMinutes > 0) ? startHour + 1 : startHour;
    self.endMinutes = (startMinutes > 0) ? 0 : 30;
    //
    self.edited = NO;
    
    if(startHour > 24)startHour -= 24;
    NSString *a = @"AM";
    if(startHour > 12)a = @"PM";
    NSString *ttle = [NSString stringWithFormat:@"Tap to add task at %d:00 %@", startHour, a];
    if(startMinutes == 30)
        ttle = [NSString stringWithFormat:@"Tap to add task at %d:30 %@", startHour, a];
    self.buttonTitle = ttle;
}

@end
