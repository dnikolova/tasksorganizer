//
//  SettingsViewController.h
//  Tasks
//
//  Created by Desislava Nikolova on 11/30/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController
{
    IBOutlet UIButton *theme1,*theme2,*theme3,*theme4;
    
    IBOutlet UISegmentedControl *weatherOnOff;
    
    int themeSelection;
    bool weatherOn;
}
@end
