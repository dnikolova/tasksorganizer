//
//  DataManager.m
//  Tasks
//
//  Created by Desislava  Nikolova on 9/16/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import "DataManager.h"
#import "AFNetworking.h"
#import <Foundation/Foundation.h>

@implementation DataManager

@synthesize locationLat, locationLong;
@synthesize loacationInfo, weatherInfo, unitsInfo, windInfo, atmosphereInfo, astronomyInfo;
@synthesize woeidLoaded;

static DataManager *m_instance = nil;


+(DataManager*)instance
{
    if(!m_instance)
    {
        m_instance = [[DataManager alloc] init];
    }
    return m_instance;
}

- (id)init
{
    self = [super init];
    woeidLoaded = false;
    doRefresh = false;
    return self;
}


-(void)refreshWeather
{
    woeidLoaded = false;
    doRefresh = true;
    [self getWeatherWoeid];
}

-(void)getWeatherWoeid
{
    if(woeidLoaded)
        return;
    
    NSLog(@"requesting the woeid");
    
    weatherOpeartion = WOEID_OPERATION;
    
    NSString *query = [NSString stringWithFormat:@"http://where.yahooapis.com/geocode?location=%f,%f&flags=J&gflags=R&appid=dj0yJmk9dmlsSDJNNlBqWTNZJmQ9WVdrOVltMTFabkZWTjJNbWNHbzlNVFl5T0RVM01EUTJNZy0tJnM9Y29uc3VtZXJzZWNyZXQmeD04MA--", locationLat, locationLong];
    NSURL *url = [NSURL URLWithString:query];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // Create Connection.
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (conn) {
        // The connection was established.
        receivedData = [NSMutableData data];
        //NSLog( @"Data will be received from URL: %@", request.URL );
    }
    else
    {
        // The download could not be made.
        //NSLog( @"Data could not be received from: %@", request.URL );
    }
    
    // PROBLEM - receivedString is NULL here.
    NSLog( @"From getContentURL: %@", query );
}

-(void)loadWeather
{
    
    weatherOpeartion = CONDITIONS_OPERATION;
    NSString *_reqString = [NSString stringWithFormat:@"http://api.worldweatheronline.com/free/v1/weather.ashx?key=8hpzub2jt7nhvevtq3yhe9fj&q=%f,%f&cc=no&date=2010-04-23&format=xml", locationLat, locationLong];
    NSURL *url = [NSURL URLWithString:_reqString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // Create Connection.
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (conn) {
        // The connection was established.
        receivedData = [NSMutableData data];
        woeidLoaded = true;
        //NSLog( @"Data will be received from URL: %@", request.URL );
    }
    else
    {
        // The download could not be made.
        //NSLog( @"Data could not be received from: %@", request.URL );
    }
    
    // PROBLEM - receivedString is NULL here.
    NSLog( @"From getContentURL: %@", receivedString );
}


-(void)connection:(NSURLConnection *)connection didReceiveResponse:
(NSURLResponse *)response
{
    // Discard all previously received data.
    [receivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:
(NSData *)data
{
    // Append the new data to the receivedData.
    [receivedData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Connection succeeded in downloading the request.
    
    // Convert received data into string.
    receivedString = [[NSString alloc] initWithData:receivedData
                                           encoding:NSASCIIStringEncoding];
    
     NSLog( @"Succeeded! Received %d bytes of data; content: %@", [receivedData length], receivedString );
    
    return;
    
    
    if(weatherOpeartion == WOEID_OPERATION)
    {
          NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:receivedData options:kNilOptions error:nil];
        NSDictionary *resultsSet = [dictionary objectForKey:@"ResultSet"];
        NSMutableArray *results = (NSMutableArray *)[resultsSet objectForKey:@"Results"];
        loacationInfo = [results objectAtIndex:0];
        //NSLog(@"From connectionDidFinishLoading: woeid results %@ %@", [dictionary description], [loacationInfo description]);
        
        /*
         city = Chicago;
         country = "United States";
         countrycode = US;
         county = "Cook County";
         countycode = "";
         hash = 0A944EBEFE9063BF;
         house = 839;
         latitude = "41.937229";
         line1 = "839 W Nelson St";
         line2 = "Chicago, IL 60657";
         line3 = "";
         line4 = "United States";
         longitude = "-87.651321";
         name = "41.936996,-87.651131";
         neighborhood = "Lake View";
         offsetlat = "41.937229";
         offsetlon = "-87.651321";
         postal = 60657;
         quality = 87;
         radius = 400;
         state = Illinois;
         statecode = IL;
         street = "W Nelson St";
         unit = "";
         unittype = "";
         uzip = 60657;
         woeid = 12784306;
         woetype = 11;
         xstreet = "";
         */
        
        woeid = [[loacationInfo objectForKey:@"woeid"] intValue];
        NSLog(@"woeid for current location %d", woeid);
        
        [self loadWeather];
        
    }else{
        NSLog( @"From connectionDidFinishLoading: wether result %@", receivedString );
        NSXMLParser *_parser = [[NSXMLParser alloc] initWithData:receivedData];
        _parser.delegate = self;
        [_parser parse];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //err
}

-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    if(weatherOpeartion == CONDITIONS_OPERATION)
    {
        if(doRefresh)
        {
            doRefresh = false;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"WeatherInfoRefresh" object:nil];
            return;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"WeatherInfoLoaded" object:nil];
    }
}


-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if(weatherOpeartion == CONDITIONS_OPERATION)
    {
        NSLog(@"el title %@", elementName);
        if([elementName isEqualToString:@"yweather:condition"])
        {
            currentElementValue = [[NSMutableString alloc] init];
            NSLog(@"weather is %@", [attributeDict objectForKey:@"text"]);
            NSLog(@"temp is %@", [attributeDict objectForKey:@"temp"]);
            NSLog(@"code is %@", [attributeDict objectForKey:@"code"]);
            
            weatherInfo = [NSDictionary dictionaryWithDictionary:attributeDict];
        }else if([elementName isEqualToString:@"pubDate"])
        {
            
        }else if([elementName isEqualToString:@"yweather:units"])
        {
            unitsInfo = [NSDictionary dictionaryWithDictionary:attributeDict];
        }else if([elementName isEqualToString:@"yweather:wind"])
        {
            windInfo = [NSDictionary dictionaryWithDictionary:attributeDict];
        }else if([elementName isEqualToString:@"yweather:atmosphere"])
        {
            atmosphereInfo = [NSDictionary dictionaryWithDictionary:attributeDict];
        }else if([elementName isEqualToString:@"yweather:astronomy"])
        {
            astronomyInfo = [NSDictionary dictionaryWithDictionary:attributeDict];
        }
        /*
        <yweather:location city="Chicago" region="IL"   country="United States"/>
        <yweather:units temperature="F" distance="mi" pressure="in" speed="mph"/>
        <yweather:wind chill="29"   direction="250"   speed="6" />
        <yweather:atmosphere humidity="52"  visibility="10"  pressure="30.11"  rising="0" />
        <yweather:astronomy sunrise="6:33 am"   sunset="5:34 pm"/>
         */
    }
}

/*
 0 	tornado
 1 	tropical storm
 2 	hurricane
 3 	severe thunderstorms
 4 	thunderstorms
 5 	mixed rain and snow
 6 	mixed rain and sleet
 7 	mixed snow and sleet
 8 	freezing drizzle
 9 	drizzle
 10 	freezing rain
 11 	showers
 12 	showers
 13 	snow flurries
 14 	light snow showers
 15 	blowing snow
 16 	snow
 17 	hail
 18 	sleet
 19 	dust
 20 	foggy
 21 	haze
 22 	smoky
 23 	blustery
 24 	windy
 25 	cold
 26 	cloudy
 27 	mostly cloudy (night)
 28 	mostly cloudy (day)
 29 	partly cloudy (night)
 30 	partly cloudy (day)
 31 	clear (night)
 32 	sunny
 33 	fair (night)
 34 	fair (day)
 35 	mixed rain and hail
 36 	hot
 37 	isolated thunderstorms
 38 	scattered thunderstorms
 39 	scattered thunderstorms
 40 	scattered showers
 41 	heavy snow
 42 	scattered snow showers
 43 	heavy snow
 44 	partly cloudy
 45 	thundershowers
 46 	snow showers
 47 	isolated thundershowers
 3200 	not available
 */

/*Weather info - if it is today query, else use the stored info */

-(NSMutableArray*)weatherIconForCode:(int)code
{
    NSMutableArray *arr =[[NSMutableArray alloc] init];
   
    if(code == 0 || code == 1)[arr addObject:@"storms.png"];
    if(code == 3)[arr addObject:@"thunderstorms01.png"];
    if(code == 4)[arr addObject:@"thunderstorms01.png"];
    if(code == 5)
    {
       [arr addObject:@"snow.png"];
       [arr addObject:@"rain03.png"];
    }
    if(code == 6)
    {
        [arr addObject:@"sleet.png"];
        [arr addObject:@"rain03.png"];
    }
    if(code == 7)
    {
        [arr addObject:@"sleet.png"];
        [arr addObject:@"snow.png"];
    }
    if(code == 10)[arr addObject:@"frost.png"];
    if(code == 11 || code == 12)[arr addObject:@"rain02.png"];
    if(code == 13)[arr addObject:@"snow01.png"];
    if(code == 14 || code == 16)[arr addObject:@"snow.png"];
    if(code == 15)
    {
        [arr addObject:@"windy.png"];
        [arr addObject:@"snow.png"];
    }
    if(code == 18)[arr addObject:@"sleet.png"];
    if(code == 20)[arr addObject:@"fog.png"];
    if(code == 21)[arr addObject:@"hazy.png"];
    if(code == 24)[arr addObject:@"windy.png"];
    if(code == 25)[arr addObject:@"freezing.png"];
    if(code == 26 || code == 28)[arr addObject:@"cloudy.png"];
    if(code == 27 || code == 29)[arr addObject:@"cloudynight.png"];
    if(code == 31 || code == 33)[arr addObject:@"clearnight.png"];
    if(code == 32 || code == 34)[arr addObject:@"sunny.png"];
    if(code == 35)
    {
        [arr addObject:@"rain.png"];
       // [arr addObject:@"snow.png"];// hail?
    }
    if(code == 36)[arr addObject:@"hot02.png"];
    if(code == 37 || code == 38 || code == 39  || code == 45   || code == 47)[arr addObject:@"thunderstorms01.png"];
    if(code == 40)[arr addObject:@"rain02.png"];
    if(code == 41 || code == 43)[arr addObject:@"snow01.png"];
    if(code == 42 || code == 46)[arr addObject:@"snow.png"];
    if(code == 44 || code == 30)[arr addObject:@"partlycloudy.png"];
    if(code == 3200)[arr addObject:@"unknown.png"];
    /*
   
     3200 	not available
     
     //
     2 	hurricane
     8 	freezing drizzle
     9 	drizzle
     19 	dust
     //
     22 	smoky
     23 	blustery
     //
     */
    
    return arr;
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if([elementName isEqualToString:@"yweather:condition"])
    {
        NSLog(@"title is %@", currentElementValue);
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    
    if(currentElementValue)
        currentElementValue = [currentElementValue stringByAppendingString:string];
}

@end
