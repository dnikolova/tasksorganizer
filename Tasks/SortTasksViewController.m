//
//  SortTasksViewController.m
//  Tasks
//
//  Created by Desislava Nikolova on 12/11/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import "SortTasksViewController.h"

@interface SortTasksViewController ()

@end

@implementation SortTasksViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)prioritySortHigh:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PrioritySort" object:@"high"];
}

-(IBAction)prioritySortLow:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PrioritySort" object:@"low"];
}


-(IBAction)featureSortCompleted:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FeatureSort" object:@"completed"];
}

-(IBAction)featureSortStatusPending:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FeatureSort" object:@"status_pending"];
}

-(IBAction)smartButtonPopular:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SmartButon" object:@"popular"];
}

-(IBAction)smartButtonFailing:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SmartButon" object:@"failing"];
}

@end
