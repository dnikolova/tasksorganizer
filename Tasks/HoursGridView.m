//
//  HoursGridView.m
//  Tasks
//
//  Created by Desislava  Nikolova on 9/16/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import "HoursGridView.h"
#import "HourLabel.h"
#import "Task.h"
#import "AppData.h"

#define kButtonHei 55

@implementation HoursGridView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        //[self addHours];
        [self updateGridView];
        [self setNeedsDisplay];
    }
    return self;
}

/* Inital set up - no data for the day */

-(void)addHours
{
    bool frac = false;
    while (self.subviews.count < 48) 
    {
        
        HourLabel *hLabel = [[HourLabel alloc] initWithFrame:CGRectMake(2, self.subviews.count * kButtonHei, self.frame.size.width, kButtonHei)];
        int hour = (int)(self.subviews.count/2) + 6;
        if(hour > 24)hour -= 24;
        NSString *a = @"AM";
        if(hour > 12)a = @"PM";
        NSString *title = [NSString stringWithFormat:@"Tap to add task at %d:00 %@", hour, a];
        if(frac)
          title = [NSString stringWithFormat:@"Tap to add task at %d:30 %@", hour, a]; 
        [hLabel setTitle:title forState:UIControlStateNormal];
        [hLabel setTintColor:[UIColor colorWithRed:110.0/225.0 green:110.0 /225.0 blue:11.0/225.0 alpha:1.0]];
        [hLabel addTarget:self action:@selector(newTask:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:hLabel];
        
        //save the time:
        hLabel.hour = hour;
        hLabel.minute = (frac) ? 30 : 0;
        
        frac = !frac;
        
        //Create empty tasks:
        Task *newEmptyTask = [[Task alloc] init];
        newEmptyTask.startHour = hour;
        newEmptyTask.startMinutes = (frac) ? 30 : 0;
        newEmptyTask.buttonTitle = [NSString stringWithString:title];
        
        hLabel.source = newEmptyTask;
        
        //store:
        [[AppData instance] saveEntryTask:newEmptyTask];
         
        
    }
}

-(void)updateGridView
{
    for(int i = self.subviews.count - 1; i >= 0; i--)
        [[self.subviews objectAtIndex:i] removeFromSuperview];
    
    //redraw from the app data:
    float offset = 0;
    for(int i = 0; i < [AppData instance].tasks.count; i++)
    {
        Task *t = [[AppData instance].tasks objectAtIndex:i];
        //to do
        float taskDuration = 1.0;
        
        if(t.edited)
        {
            taskDuration = 2.0;
        }
        
        HourLabel *hLabel = [[HourLabel alloc] initWithFrame:CGRectMake(2, self.subviews.count * kButtonHei + offset, self.frame.size.width, kButtonHei * taskDuration)];
        
        hLabel.source = t;
        
        if(t.edited)
        {
            [hLabel setTitle:@"" forState:UIControlStateNormal];
            [hLabel config];
        }else{
           [hLabel setTitle:t.buttonTitle forState:UIControlStateNormal];
           [hLabel addTarget:self action:@selector(newTask:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        hLabel.hour = t.startHour;
        hLabel.minute = t.startMinutes;
        
        [self addSubview:hLabel];
        
        if(t.edited)
            offset += kButtonHei * (taskDuration - 1);
        
        
    }
    
    offset = self.subviews.count * kButtonHei + offset;
    
    NSNumber *hei = [NSNumber numberWithFloat:offset];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetGridScrollViewSize" object:hei];
}

-(void)newTask:(id)sender
{
    _task = (HourLabel*)sender;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewTaskEntry" object:_task];
}

-(void)updateBtn
{
    [_task setTitle:@"Edited" forState:UIControlStateNormal];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
