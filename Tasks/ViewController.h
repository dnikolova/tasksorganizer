//
//  ViewController.h
//  Tasks
//
//  Created by Desislava  Nikolova on 9/15/12.
//  Copyright (c) 2012 ScopicSoftware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManager.h>
#import "HoursGridView.h"
#import "NewTaskViewController.h"
#import "Task.h"
#import "CalenderSelectorView.h"
#import "HelpViewController.h"

@class NewTaskViewController;
@class SettingsViewController;
@class TaskStatusViewController;
@class SortTasksViewController;
@class WeatherMoreViewController;
@class WeatherInfoView;

@interface ViewController : UIViewController<NSFetchedResultsControllerDelegate,CLLocationManagerDelegate, UIAlertViewDelegate, UIPickerViewDataSource,UIPickerViewDelegate>
{
    IBOutlet UIView *tasksView;
    
    IBOutlet UIView *tasks1;
    IBOutlet UIView *tasks2;
    IBOutlet UIView *tasks3;
    IBOutlet UIView *tasks4;
    
    IBOutlet UIScrollView *_scrollView;
    HoursGridView *_hoursGridView;
    HoursGridView *_freeTimeGridView;
    
    NewTaskViewController *_newTaskViewController;
    SettingsViewController *_settingsViewController;
    TaskStatusViewController *_taskStatusViewController;
    SortTasksViewController *_sortTasksViewController;
    
    WeatherMoreViewController *_weatherMoreViewController;
    
    Task *_newTask;
    
    IBOutlet WeatherInfoView *_weatherView;
    
    IBOutlet UIButton *_next, *_prev;
    
    CLLocation *currentLocation;
    CLLocationManager *locationManager;

    bool nextDay, prevDay;
    
    IBOutlet UIDatePicker *_pickerView;
    float pickerYPos;
    IBOutlet UIToolbar *_toolBar;
    float toolbarYPos;
    IBOutlet UIView *_dateView;
    float dateViewYPos;
    
    IBOutlet CalenderSelectorView *calender;
    
    HelpViewController *_howTo;
    
    int dayBefore;
}

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

-(IBAction)newTaskBtnAction;
-(IBAction)howToBtnAction;
-(void)saveNewTask:(Task*)task;
-(void)cancelNewTask;
//
-(void)showMoreWeather;


@end
