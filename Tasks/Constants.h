//
//  Constants.h
//  Tasks
//
//  Created by Desislava Nikolova on 1/12/13.
//  Copyright (c) 2013 DesiNikolova. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#ifndef Tasks_Constants_h
#define Tasks_Constants_h

#define kWeatherAnimationDelay 5.5

#define kTheme1_ColorP1 [UIColor colorWithRed:166.0/255.0 green:0.0/255.0 blue:255.0/255.0 alpha:0.44].CGColor
#define kTheme1_ColorP2 [UIColor colorWithRed:0.0/255.0 green:34.0/255.0 blue:255.0/255.0 alpha:0.44].CGColor
#define kTheme1_ColorP3 [UIColor colorWithRed:0.0/245.0 green:208.0/255.0 blue:255.0/255.0 alpha:0.44].CGColor

#define kTheme2_ColorP1 [UIColor colorWithRed:240.0/255.0 green:19.0/255.0 blue:19.0/255.0 alpha:0.44].CGColor 
#define kTheme2_ColorP2 [UIColor colorWithRed:204.0/255.0 green:133.0/255.0 blue:133.0/255.0 alpha:0.44].CGColor 
#define kTheme2_ColorP3 [UIColor colorWithRed:240.0/245.0 green:179.0/255.0 blue:179.0/255.0 alpha:0.44].CGColor 

#define kTheme3_ColorP1 [UIColor colorWithRed:240.0/255.0 green:168.0/255.0 blue:0.0/255.0 alpha:0.44].CGColor
#define kTheme3_ColorP2 [UIColor colorWithRed:255.0/255.0 green:183.0/255.0 blue:15.0/255.0 alpha:0.44].CGColor
#define kTheme3_ColorP3 [UIColor colorWithRed:255.0/245.0 green:226.0/255.0 blue:61.0/255.0 alpha:0.44].CGColor

#define kTheme4_ColorP1 [UIColor colorWithRed:19.0/255.0 green:207.0/255.0 blue:97.0/255.0 alpha:0.44].CGColor
#define kTheme4_ColorP2 [UIColor colorWithRed:35.0/255.0 green:222.0/255.0 blue:110.0/255.0 alpha:0.44].CGColor
#define kTheme4_ColorP3 [UIColor colorWithRed:82.0/245.0 green:255.0/255.0 blue:154.0/255.0 alpha:0.44].CGColor

#define kTheme_Task_Done [UIColor colorWithRed:66.0/245.0 green:66.0/255.0 blue:66.0/255.0 alpha:0.44].CGColor

#define kTopicsArray [[NSMutableArray alloc] initWithObjects: @"brief_case.png",@"piggy_bank.png",@"people_family.png", @"heart.png",@"house.png",@"pacman.png",@"lunch.png", @"working.png",nil]

#define kTopicsNames [[NSMutableArray alloc] initWithObjects:@"Travel", @"At the Bank", @"With Family", @"Dating",@"At Home", @"Happy Hour", @"Eating", @"Working", nil]

#endif


