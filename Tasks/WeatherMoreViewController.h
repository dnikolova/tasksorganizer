//
//  WeatherMoreViewController.h
//  Tasks
//
//  Created by Desislava Nikolova on 1/5/13.
//  Copyright (c) 2013 DesiNikolova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherMoreViewController : UIViewController<UIAlertViewDelegate>
{
    IBOutlet UIImageView *_weatherIcon1,*_weatherIcon2;
    IBOutlet UILabel *_degrees, *_condistions, *_locationAndTime;
    
    IBOutlet UITextView *_moreDetails, *_atmoDetails, *_daytimeDetails;
}

-(void)updateWeather;

@end
