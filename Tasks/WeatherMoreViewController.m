//
//  WeatherMoreViewController.m
//  Tasks
//
//  Created by Desislava Nikolova on 1/5/13.
//  Copyright (c) 2013 DesiNikolova. All rights reserved.
//

#import "WeatherMoreViewController.h"
#import "DataManager.h"

@interface WeatherMoreViewController ()

@end

@implementation WeatherMoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateWeather];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertView.title isEqualToString:@"Weather unavailable"])
    {
        if(buttonIndex == 1)
        {
            [[DataManager instance]  refreshWeather];
        }
    }
}

-(void)updateWeather
{
    if([[DataManager instance].weatherInfo objectForKey:@"code"] == nil)
    {
        _degrees.hidden = YES;
        _condistions.hidden = YES;
        _locationAndTime.hidden = YES;
        
        _moreDetails.hidden = YES;
        _atmoDetails.hidden = YES;
        _daytimeDetails.hidden = YES;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Weather unavailable" message:@"No weather data is currently available. Refresh the service or try later" delegate:self cancelButtonTitle:@"Try later" otherButtonTitles:@"Refresh", nil];
        [alert show];
        
        return;
    }
    NSLog(@"updating the weather");
    _degrees.hidden = NO;
    _condistions.hidden = NO;
    _locationAndTime.hidden = NO;
    
    _moreDetails.hidden = NO;
    _atmoDetails.hidden = NO;
    _daytimeDetails.hidden = NO;
    
    int code = [[[DataManager instance].weatherInfo objectForKey:@"code"] intValue];
    NSMutableArray *icons = [[DataManager instance] weatherIconForCode:code ];
    if(icons.count > 0)
        _weatherIcon1.image = [UIImage imageNamed:[icons objectAtIndex:0]];
    if(icons.count > 1)
        _weatherIcon2.image = [UIImage imageNamed:[icons objectAtIndex:1]];
    _degrees.text  = [NSString stringWithFormat:@"%@ %@ F",[[DataManager instance].weatherInfo objectForKey:@"temp"], @"\u00B0"];
    _condistions.text =[[DataManager instance].weatherInfo objectForKey:@"text"];
    NSString *time = [[DataManager instance].weatherInfo objectForKey:@"date"] ;
    NSString *hour = [time substringFromIndex:17];
    _locationAndTime.text = [NSString stringWithFormat:@" %@ %@, %@", [[DataManager instance].loacationInfo objectForKey:@"city"], [[DataManager instance].loacationInfo objectForKey:@"state"], hour];
    //other info:
    /*
     <yweather:location city="Chicago" region="IL"   country="United States"/>
     <yweather:units temperature="F" distance="mi" pressure="in" speed="mph"/>
     <yweather:wind chill="29"   direction="250"   speed="6" />
     <yweather:atmosphere humidity="52"  visibility="10"  pressure="30.11"  rising="0" />
     <yweather:astronomy sunrise="6:33 am"   sunset="5:34 pm"/>
     */
    NSMutableString *_moreStr = [NSString stringWithFormat:@"Wind chill - %@ \nWind direction - %@ \nWind speed - %@ \n\n", [[DataManager instance].windInfo objectForKey:@"chill"], [[DataManager instance].windInfo objectForKey:@"direction"], [[DataManager instance].windInfo objectForKey:@"speed"]];
    NSMutableString *_atmoStr = [NSString stringWithFormat:@"Humidity - %@ \nVisibility - %@ \nPressured - %@ \n\n", [[DataManager instance].atmosphereInfo objectForKey:@"humidity"], [[DataManager instance].atmosphereInfo objectForKey:@"visibility"], [[DataManager instance].atmosphereInfo objectForKey:@"pressure"]];
    NSMutableString *_astroStr = [NSString stringWithFormat:@"Sunrise - %@\nSunset - %@", [[DataManager instance].astronomyInfo objectForKey:@"sunrise"], [[DataManager instance].astronomyInfo objectForKey:@"sunset"]];
    _moreDetails.text = _moreStr;
    _atmoDetails.text = _atmoStr;
    _daytimeDetails.text = _astroStr;
}

-(IBAction)cancelWeather
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseModalView" object:nil];
}

-(IBAction)refresh:(id)sender
{
    [[DataManager instance]  refreshWeather];
}

@end
