//
//  HelpViewController.m
//  Tasks
//
//  Created by Desislava Nikolova on 4/3/13.
//  Copyright (c) 2013 DesiNikolova. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self newtask:nil];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)navigation:(id)sender
{
    currentView = 2;
    instructionsView.text = @"Use the arrows on the top to navigate to previous/next date or jump to date by using the 'Go to' button on the bottom.";
    _img.image = [UIImage imageNamed:@"navigation.png"];
    //_prevBtn.hidden = YES; _nextBtn.hidden = YES;
    
}

-(IBAction)newtask:(id)sender
{
    currentView = 1;
    instructionsView.text = @"Tap on a free time slot on the main app screen to add a new task.";
    _img.image = [UIImage imageNamed:@"newtask.png"];
    //_prevBtn.hidden = NO; _nextBtn.hidden = NO;
}

-(IBAction)settings:(id)sender
{
    currentView = 3;
    instructionsView.text = @"Choose between 4 different color themes for the main tasks' display.";
    _img.image = [UIImage imageNamed:@"settings1.png"];
    //_prevBtn.hidden = NO; _nextBtn.hidden = NO;
}


-(IBAction)cancelBtnAction
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CancelHelpView" object:nil];
}

-(IBAction)prevBtnAction
{
    if(currentView == 1)
    {
        currentView = 3;
        [self showView];
    }
    currentView --;
    [self showView];
    
}

-(IBAction)nextBtnAction;
{
    if(currentView == 3)
    {
        currentView = 1;
        [self showView];
    }
    currentView ++;
    [self showView];
}
     
-(void)showView
{
    if(currentView == 1)
        [self newtask:nil];
    else if(currentView == 2)
        [self navigation:nil];
    else if(currentView == 3)
        [self settings:nil];
}

@end
