//
//  AppData.h
//  Tasks
//
//  Created by Desislava Nikolova on 3/3/13.
//  Copyright (c) 2013 DesiNikolova. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Task.h"

@interface AppData : NSObject
{
    int _tasksIDs;
    int day, month, year; // current date
    Task *dbTask;
}

@property(nonatomic,retain)Task *workingTask;
@property(nonatomic,retain)NSMutableArray *dbTasks;
@property(nonatomic,retain)Task *targetTask; // placeholder for single entry query from the DB - temp
@property(nonatomic, retain) NSMutableArray *tasks, *topicsArray, *topicsNames;
@property(nonatomic, retain) NSDate *jumpToDate;
@property(nonatomic, assign) int themeColor;
@property(nonatomic, assign) bool weatherOn;

+(AppData*)instance;

//managing tasks:

-(void)retrieveTasks;
-(void)nextDay;
-(void)prevDay;
-(void)saveEntryTask:(Task*)task;
-(void)saveTask;
-(void)updateTask;
-(void)deleteTask:(Task*)task;
-(void)saveTaskChanges;
-(void)showJumpToDate;
-(void)availableTimeSlots;
-(void)showFreeTimeForCurrentDate;
-(int)getDay:(BOOL)next;
-(int)currentDay;
//data query - utils:

-(BOOL)isTimeAvailable:(int)hour minutes:(int)minutes date:(NSString*)date;
@end
