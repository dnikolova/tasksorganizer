//
//  TaskLabel.m
//  Tasks
//
//  Created by Desislava Nikolova on 3/4/13.
//  Copyright (c) 2013 DesiNikolova. All rights reserved.
//

#import "TaskLabel.h"
#import "AppData.h"

@implementation TaskLabel

@synthesize fill, stroke, cornerRadius, drawingTextColor, drawsBorder, fontSize, drawingTextString, textAlignmentCenter,completed;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.fill = [UIColor greenColor];
        self.stroke = [UIColor lightGrayColor];
        self.cornerRadius = 8.0;
        self.drawingTextColor = [UIColor whiteColor];
        if([AppData instance].themeColor > 2)
             self.drawingTextColor = [UIColor blackColor];
        self.drawsBorder = YES;
        self.textAlignmentCenter = YES;
        self.fontSize = 14;
        self.boldFont = NO;
        self.drawingTextString = @"";
        self.backgroundColor = [UIColor clearColor];
        self.completed = NO;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if(drawsBorder)
    {
        // Drawing with a white stroke color
        CGContextRef context=UIGraphicsGetCurrentContext();
        CGContextSetRGBStrokeColor(context, 1.0, 1.0, 1.0, 1.0);
        if([AppData instance].themeColor > 2)
            CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0);
        if(completed)
        {
            CGContextSetRGBFillColor(context, 82.0/255.0, 107.0/255.0, 133.0/255.0, 0.0);
        }else{
            CGContextSetRGBFillColor(context, 207.0/255.0, 145.0/255.0, 107.0/255.0, 0.0);
        }
    
        CGRect rrect = CGRectMake(3.0, 3.0, self.frame.size.width - 7.0, self.frame.size.height - 6.0);
        CGFloat radius = cornerRadius;
        // 
        CGFloat minx = CGRectGetMinX(rrect), midx = CGRectGetMidX(rrect), maxx = CGRectGetMaxX(rrect);
        CGFloat miny = CGRectGetMinY(rrect), midy = CGRectGetMidY(rrect), maxy = CGRectGetMaxY(rrect);
    
        // Start at 1
        CGContextMoveToPoint(context, minx, midy);
        // Add an arc through 2 to 3
        CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
        // Add an arc through 4 to 5
        CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);
        // Add an arc through 6 to 7
        CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
        // Add an arc through 8 to 9
        CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);
        // Close the path
        CGContextClosePath(context);
        // Fill & stroke the path
        CGContextDrawPath(context, kCGPathFillStroke);
    }
    
    NSString *text = self.drawingTextString;
    CGPoint center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
    UIFont *font = [UIFont systemFontOfSize:self.fontSize];
    if(self.boldFont)
        font = [UIFont boldSystemFontOfSize:self.fontSize];
    
    CGSize stringSize = [text sizeWithFont:font];
    CGRect stringRect = CGRectMake(center.x-stringSize.width/2, center.y-stringSize.height/2, stringSize.width, stringSize.height);
    if(!textAlignmentCenter)
    {
        stringRect = CGRectMake(2.0, center.y-stringSize.height/2, stringSize.width, stringSize.height);
    }
    //[[UIColor blackColor] set];
    //CGContextFillRect(context, stringRect);
    
    [drawingTextColor set];
    [text drawInRect:stringRect withFont:font];

}


@end
