//
//  NewTaskViewController.m
//  Tasks
//
//  Created by Desislava Nikolova on 11/25/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import "NewTaskViewController.h"
#import "ViewController.h"
#import "AppData.h"

@class ViewController;

@interface NewTaskViewController ()

@end



@implementation NewTaskViewController

@synthesize delegate;
@synthesize taskName,taskDesc,taskPriority;
@synthesize startTime, endTime;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)loadView
{
    [super loadView];
    
    topicsArray = [AppData instance].topicsArray;
    
    topicsNames = [AppData instance].topicsNames;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    taskName.delegate = self; taskDesc.delegate = self;
    startTime.delegate = self; endTime.delegate = self;
    
    _pickerView.showsSelectionIndicator = YES;
    _topicPickerView.showsSelectionIndicator = YES;
    
    pickerYPos = _pickerView.frame.origin.y;
    
    _pickerView.hidden = YES;
    _topicPickerView.hidden = YES;
    
    loadingTopics = false;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _done.hidden = YES;
    
    //configure:
    int hr =  [AppData instance].workingTask.startHour;
    int min = [AppData instance].workingTask.startMinutes;
    NSLog(@"configuring the task start hr: %d start min: %d", hr, min);
    if(min < 10)
    {
        startTime.text = [NSString stringWithFormat:@"%d:0%d",hr, min ];
    }else{
        startTime.text = [NSString stringWithFormat:@"%d:%d",hr, min ];
    }
    
    if(hr == 24)
        hr = 0;
    hr += 1;
    if(min < 10)
    {
       endTime.text = [NSString stringWithFormat:@"%d:0%d",hr, min ];
    }else{
        endTime.text = [NSString stringWithFormat:@"%d:%d",hr, min ];
    }
    
    //init config:
    [AppData instance].workingTask.endHour = hr;
    [AppData instance].workingTask.endMinutes = min;
    
}

-(IBAction)taskTopicBtnAction
{
    NSLog(@"topics");
    loadingTopics = true;
    [taskDesc resignFirstResponder];
    [taskName resignFirstResponder];
    
    _topicPickerView.frame = CGRectMake(0, pickerYPos + _topicPickerView.frame.size.height, _topicPickerView.frame.size.width, _topicPickerView.frame.size.height);
    _topicPickerView.hidden = NO;
    //load the new day
    [UIView beginAnimations:@"ShowTopicIn" context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.44];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    //[UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
    CGRect f = _topicPickerView.frame;
    f.origin.y = pickerYPos;
    _topicPickerView.frame = f;
    [UIView commitAnimations];
    _done.hidden = NO;
}

-(IBAction)startTimeBtnAction
{
    [taskDesc resignFirstResponder];
    [taskName resignFirstResponder];
    editStartTime = true;
    _pickerView.frame = CGRectMake(0, pickerYPos + _pickerView.frame.size.height, _pickerView.frame.size.width, _pickerView.frame.size.height);
    _pickerView.hidden = NO;
    //load the new day
    [UIView beginAnimations:@"ShowNewDayIn" context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.44];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    //[UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
    CGRect f = _pickerView.frame;
    f.origin.y = pickerYPos;
    _pickerView.frame = f;
    [UIView commitAnimations];
    _done.hidden = NO;
}

-(IBAction)endTimeBtnAction
{
    [taskDesc resignFirstResponder];
    [taskName resignFirstResponder];
    editStartTime = false;
    [_pickerView reloadAllComponents];
    _pickerView.frame = CGRectMake(0, pickerYPos + _pickerView.frame.size.height, _pickerView.frame.size.width, _pickerView.frame.size.height);
    _pickerView.hidden = NO;
    //load the new day
    [UIView beginAnimations:@"ShowNewDayIn" context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.44];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    //[UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
    CGRect f = _pickerView.frame;
    f.origin.y = pickerYPos;
    _pickerView.frame = f;
    [UIView commitAnimations];
    _done.hidden = NO;
}

-(IBAction)saveNewTaskBtnAction
{
    if([taskName.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing title" message:@"Please, enter a title." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    [AppData instance].workingTask.title = taskName.text;
    [AppData instance].workingTask.description = taskDesc.text;
    [AppData instance].workingTask.taskPriority = 1;
    if(taskPriority.selectedSegmentIndex == 1)
       [AppData instance].workingTask.taskPriority = 2;
    if(taskPriority.selectedSegmentIndex == 2)
        [AppData instance].workingTask.taskPriority = 3;
    [AppData instance].workingTask.status = 0;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveNewTask" object:nil];
}

-(IBAction)didChangePriority:(id)sender
{
    if(taskPriority.selectedSegmentIndex == 0)
        priorityDot.image = [UIImage imageNamed:@"redBall"];
    if(taskPriority.selectedSegmentIndex == 1)
        priorityDot.image = [UIImage imageNamed:@"yellowBall"];
    if(taskPriority.selectedSegmentIndex == 2)
        priorityDot.image = [UIImage imageNamed:@"greenBall"];
}

-(IBAction)cancelBtnAction
{
   [[NSNotificationCenter defaultCenter] postNotificationName:@"CancelNewTask" object:nil];
}

/* UITextField and UITextView delegates */

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if([textField isEqual:startTime] || [textField isEqual:endTime])
        return NO;
    
    if(_pickerView.frame.origin.y == pickerYPos)
    {
        [UIView beginAnimations:@"HidePicker" context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.44];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        CGRect f = _pickerView.frame;
        f.origin.y = pickerYPos + _pickerView.frame.size.height + 10;
        _pickerView.frame = f;
        [UIView commitAnimations];
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    _done.hidden = NO;
    CGRect f = _pickerView.frame;
    f.origin.y = pickerYPos + _pickerView.frame.size.height + 10;
    _pickerView.frame = f;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    _done.hidden = NO;
    CGRect f = _pickerView.frame;
    f.origin.y = pickerYPos + _pickerView.frame.size.height + 10;
    _pickerView.frame = f;
}

-(IBAction)done
{
    [taskDesc resignFirstResponder];
    [taskName resignFirstResponder];
    _done.hidden = YES;
    
    NSLog(@" %f %f",_topicPickerView.frame.origin.y, pickerYPos);
    
    if(_pickerView.frame.origin.y == pickerYPos && !_pickerView.hidden)
    {
        [UIView beginAnimations:@"HidePicker" context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.44];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        CGRect f = _pickerView.frame;
        f.origin.y = pickerYPos + _pickerView.frame.size.height + 10;
        _pickerView.frame = f;
        [UIView commitAnimations];
    }else if(_topicPickerView.frame.origin.y > 200.0 && !_topicPickerView.hidden)
    {
        [UIView beginAnimations:@"HideTopicsPicker" context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.44];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        CGRect f = _topicPickerView.frame;
        f.origin.y = pickerYPos + _topicPickerView.frame.size.height + 10;
        _topicPickerView.frame = f;
        [UIView commitAnimations];
    }
    
}

-(void)transitionDidStop:(NSString *)animationID finished:(BOOL)finished context:(void *)context
{
    
    if([animationID isEqualToString:@"HidePicker"])
    {
        _pickerView.hidden = YES;
    }if([animationID isEqualToString:@"HideTopicsPicker"])
    {
        _topicPickerView.hidden = YES;
        int sel = [_topicPickerView selectedRowInComponent:0];
        topicImg.image = [UIImage imageNamed:[topicsArray objectAtIndex:sel]];
        topicLabel.text = [topicsNames objectAtIndex:sel];
        [AppData instance].workingTask.cateory = sel;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    if(!_topicPickerView.hidden)
    {
        int sel = [_topicPickerView selectedRowInComponent:0];
        topicImg.image = [UIImage imageNamed:[topicsArray objectAtIndex:sel]];
        topicLabel.text = [topicsNames objectAtIndex:sel];
    }else if(!_pickerView.hidden)
    {
        int hr = 0, mins = 0;
        mins = ([_pickerView selectedRowInComponent:1] == 1) ? 30 : 0;
        hr = [_pickerView selectedRowInComponent:0] + [AppData instance].workingTask.startHour + 1;
        if(hr > 24)
            hr -= 24;
        //handle the change;
        if(component == 0)
        {
            hr = row + [AppData instance].workingTask.startHour + 1;
            if(hr > 24)
                hr -= 24;
        }else{
            mins = 0;
            if(row)mins = 30;
        }
        NSString *end_time = [NSString stringWithFormat:@"0%d:%d0", hr, mins];
        if(hr > 9 && mins > 9)
            end_time = [NSString stringWithFormat:@"%d:%d", hr, mins];
        else if(hr > 9 && mins <= 9)
            end_time = [NSString stringWithFormat:@"%d:%d0", hr, mins];
        else if(hr <= 9 && mins > 9)
            end_time = [NSString stringWithFormat:@"%0d:%d", hr, mins];
        endTime.text = end_time;
        
        [AppData instance].workingTask.endHour = hr;
        [AppData instance].workingTask.endMinutes = mins;
            
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(pickerView == _topicPickerView)
    {
        return topicsArray.count;
    }
    NSUInteger numRows = 25;
    if(component == 1) numRows = 2; // 12
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if(pickerView == _topicPickerView)
    {
        return 1;
    }
    return 2;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(pickerView == _topicPickerView)
    {
        NSLog(@"empty tile");
        return @"";
    }
    NSString *title;
    if(component == 0)
    {
        int hr = row + [AppData instance].workingTask.startHour;
        if(hr > 24)
            hr -= 24;
        title = [@"" stringByAppendingFormat:@"%d o'clock",hr ];
    }else{
        title = [@"" stringByAppendingFormat:@"%d minutes",row * 5];
        if(row * 5 < 10)
            title = [@"" stringByAppendingFormat:@"%d0 minutes",row * 5];
        
    }
    
    return title;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if(pickerView == _topicPickerView)
    {
        UIImage *img = [UIImage imageNamed:[topicsArray objectAtIndex:row]];
        UIImageView *temp = [[UIImageView alloc] initWithImage:img];
        temp.contentMode = UIViewContentModeScaleAspectFill;
        temp.frame = CGRectMake(15, 15, 30, 30);
        [temp setNeedsDisplay];
        
        
        UILabel *channelLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 0, 180, 60)];
        channelLabel.text = [topicsNames objectAtIndex:row];
        channelLabel.font = [UIFont boldSystemFontOfSize:14];
        channelLabel.textColor = [UIColor whiteColor];
        channelLabel.textAlignment = UITextAlignmentLeft;
        channelLabel.backgroundColor = [UIColor clearColor];
        
        UIView *tmpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 60)];
        UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"largeButton.png"]];
        CGRect f = bg.frame;
        f.origin.y = 4;
        bg.frame = f;
        [tmpView insertSubview:bg atIndex:0];
        [tmpView insertSubview:temp atIndex:1];
        [tmpView insertSubview:channelLabel atIndex:2];
        tmpView.backgroundColor = [UIColor whiteColor];
        pickerView.backgroundColor = [UIColor whiteColor];
        return tmpView;
    }else{
        NSString *title;
        if(component == 0)
        {
            int hr = row + [AppData instance].workingTask.startHour + 1;
            if(hr > 24)
                hr -= 24;
            title = [@"" stringByAppendingFormat:@"%d o'clock",hr ];
        }else{
            title = @"00 minutes"; 
            if(row)
                title = @"30 minutes";
            /*
            title = [@"" stringByAppendingFormat:@"%d minutes",row * 5];
            if(row * 5 < 10)
                title = [@"" stringByAppendingFormat:@"%d0 minutes",row * 5];
             */
            
        }
        
        UILabel *channelLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 120, 60)];
        channelLabel.text = title;
        channelLabel.font = [UIFont boldSystemFontOfSize:14];
        channelLabel.textColor = [UIColor blackColor];
        channelLabel.textAlignment = UITextAlignmentCenter;
        channelLabel.backgroundColor = [UIColor whiteColor];
        
        UIView *tmpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 60)];
        [tmpView addSubview:channelLabel];
        
        return tmpView;
    }
    NSLog(@"return the view");
    return nil;
    
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if(pickerView == _topicPickerView)
    {
        return 300;
    }
    int sectionWidth = 140;
    if(component == 1)sectionWidth = 180;
    return sectionWidth;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if(pickerView == _topicPickerView)
    {
        return 60;
    }
    return 44;
}

@end
