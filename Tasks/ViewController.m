//
//  ViewController.m
//  Tasks
//
//  Created by Desislava  Nikolova on 9/15/12.
//  Copyright (c) 2012 ScopicSoftware. All rights reserved.
//

#import "ViewController.h"
#import "ViewUtils.h"
#import "SettingsViewController.h"
#import "TaskStatusViewController.h"
#import "SortTasksViewController.h"
#import "DataManager.h"
#import "AppData.h"
#import "WeatherMoreViewController.h"
#import "WeatherInfoView.h"
#import "HourLabel.h"
#import "TaskEntry.h"

@interface ViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end


@implementation ViewController

@synthesize fetchedResultsController=_fetchedResultsController, managedObjectContext=_managedObjectContext;

-(void)loadView
{
    [super loadView];
    
    /* Check the virtual database tables - create if there are none */
    
    [[AppData instance] retrieveTasks];
  
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(animateTasksIn) name:@"TasksViewDisplayed" object:nil];
    
    [ViewUtils animateViewIn:tasksView fromDirection:YES];
    

}


- (void)viewDidUnload
{
    // Release any properties that are loaded in viewDidLoad or can be recreated lazily.
    self.fetchedResultsController = nil;
}


-(void)animateTasksIn
{
    /*
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New task" message:@"You have no tasks for the day! Tap on the hours grid to add a task." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
    */
    _hoursGridView = [[HoursGridView alloc] initWithFrame:CGRectMake(0,0,320,2200)];
    [_scrollView addSubview:_hoursGridView];
    [_scrollView setContentSize:CGSizeMake(_scrollView.contentSize.width, 2250)];
    
    _freeTimeGridView = [[HoursGridView alloc] initWithFrame:CGRectMake(0,0,320,2200)];
    
    CGRect f = _hoursGridView.frame;
    f.origin.y = - 550;
    _hoursGridView.alpha = 0;
    //load the new day
    [UIView beginAnimations:@"GridIn" context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.33];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
    //[_hoursGridView setTransform:CGAffineTransformMakeScale(0.77,0.77)];
    
    _hoursGridView.alpha = 1.0;
    [UIView commitAnimations];
  
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    _newTaskViewController = [[NewTaskViewController alloc] initWithNibName:@"NewTaskView" bundle:[NSBundle mainBundle]];
    _newTaskViewController.delegate = self;
    
    _settingsViewController = [[SettingsViewController alloc] initWithNibName:@"SettingsView" bundle:[NSBundle mainBundle]];
    
    _taskStatusViewController = [[TaskStatusViewController alloc] initWithNibName:@"EditTaskView" bundle:[NSBundle mainBundle]];
    
    _sortTasksViewController = [[SortTasksViewController alloc] initWithNibName:@"SortTasksView" bundle:[NSBundle mainBundle]];
    
    _weatherMoreViewController = [[WeatherMoreViewController alloc]initWithNibName:@"WeatherMoreView" bundle:[NSBundle mainBundle]];
    
    _howTo = [[HelpViewController alloc]initWithNibName:@"HelpView" bundle:[NSBundle mainBundle]];
    
    
    pickerYPos = _pickerView.frame.origin.y;
    toolbarYPos = _toolBar.frame.origin.y;
    dateViewYPos = _dateView.frame.origin.y;
    _dateView.hidden = YES;
    
     [calender goToPage:[[AppData instance] currentDay] flipUp:YES];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(saveNewTask:)name:@"SaveNewTask" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(cancelNewTask)name:@"CancelNewTask" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(cancelTheme)name:@"CancelTheme" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(saveTheme)name:@"SaveTheme" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(closeModalView)name:@"CancelHelpView" object:nil];
    
    //////
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(prioritySort:)name:@"PrioritySort" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(featureSort:)name:@"FeatureSort" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(smartButton:)name:@"SmartButon" object:nil];
  
    ///
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(newTask:)name:@"NewTaskEntry" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(editTask:)name:@"EditTaskEntry" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(closeModalView)name:@"CancelEditTask" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(saveEditTask)name:@"SaveEditTask" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(deleteTask:)name:@"DeleteTaskEntry" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(closeModalView)name:@"CloseModalView" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(showCalenderSelection:)name:@"ShowCalenderSelection" object:nil];
    
    //Weather:
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(weatherInfoLoaded)name:@"WeatherInfoLoaded" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(refreshWeather)name:@"WeatherInfoRefresh" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(setGridScrollViewSize:)name:@"SetGridScrollViewSize" object:nil];
    
    //
    
    //Location information:
    currentLocation = [[CLLocation alloc] init];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    [self start];
    
}

-(void)weatherInfoLoaded
{
    [_weatherView updateWeather];
}

-(void)refreshWeather
{
    if([[DataManager instance].weatherInfo objectForKey:@"code"] == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Weather unavailable" message:@"Weather data still unavailable" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }
    [_weatherMoreViewController updateWeather];
    [_weatherView updateWeather];
}

-(void)prioritySort:(NSNotification*)notif
{
     [self dismissModalViewControllerAnimated:YES];
}


-(void)featureSort:(NSNotification*)notif
{
    [self dismissModalViewControllerAnimated:YES]; 
}


-(void)smartButton:(NSNotification*)notif
{
     [self dismissModalViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if(interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        return YES;
    return NO;
}

-(IBAction)newTaskBtnAction
{
    [self presentModalViewController:_newTaskViewController animated:YES];
}

-(IBAction)settingsBtnAction
{
    [self presentModalViewController:_settingsViewController animated:YES];
}

-(IBAction)sortBtnAction
{
    [self presentModalViewController:_sortTasksViewController animated:YES];
}

-(void)editTask:(NSNotification*)notif
{
    Task *t = (Task*)notif.object;
    [AppData instance].workingTask = t;
    [self presentModalViewController:_taskStatusViewController animated:YES];
}

-(void)saveEditTask
{
    [[AppData instance] saveTaskChanges];
    [_hoursGridView updateGridView];
    [self closeModalView];
}

-(void)deleteTask:(NSNotification*)notif
{
    Task *t = (Task*)notif.object;
    [AppData instance].workingTask = t;
    UIAlertView *al = [[UIAlertView alloc] initWithTitle:@"Delete task" message:@"Are you sure you want to delete the task? This action will be permanent." delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [al show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertView.title isEqualToString:@"Delete task"])
    {
        if(buttonIndex == 1)
        {
            [[AppData instance] deleteTask:[AppData instance].workingTask];
            [_hoursGridView updateGridView];
        }
    }
}

-(void)newTask:(NSNotification*)notif
{
    HourLabel *_hLbl = (HourLabel*)notif.object;
    
    _newTask = _hLbl.source;
    _newTask.startHour = _hLbl.hour;
    _newTask.startMinutes = _hLbl.minute;
    NSLog(@"n t %d %d", _newTask.startHour,_newTask.startMinutes );
    //Set a global reference for easier access:
    [AppData instance].workingTask = _hLbl.source;//_newTask;
    
    [self newTaskBtnAction];
    //config
}

-(void)saveNewTask:(Task*)task
{
    [self dismissModalViewControllerAnimated:YES];
    
    [AppData instance].workingTask.edited = YES;
    
    [[AppData instance] updateTask];
   
    [_hoursGridView updateGridView];
    
    [_hoursGridView updateBtn];//using the new task
}
-(void)cancelNewTask
{
    [self dismissModalViewControllerAnimated:YES];
}
-(void)saveTheme
{
    [_hoursGridView updateGridView];
    _weatherView.hidden = ![AppData instance].weatherOn;
    [self dismissModalViewControllerAnimated:YES];
}
-(void)cancelTheme
{
    [self dismissModalViewControllerAnimated:YES];
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(void)showMoreWeather
{
    [self presentModalViewController:_weatherMoreViewController animated:YES];
}


-(void)closeModalView
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark Current location detection

-(void) start {
    NSLog(@"started location updates");
    [locationManager startUpdatingLocation];
}

-(void) stop {
    [locationManager stopUpdatingLocation];
}

-(BOOL) locationKnown {
    float sp = currentLocation.speed;
    NSLog(@"speed %f", currentLocation.speed);
    if (sp < -1)
        return NO;
    else
        return YES;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    //if the time interval returned from core location is more than two minutes we ignore it because it might be from an old session
    //if ( abs([newLocation.timestamp timeIntervalSinceDate: [NSDate date]]) < 120) {
        currentLocation = [[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];//newLocation;
        NSLog(@"location is %f %f", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
        
        [self stop];
        
        [self loadWatherInfo];
        
    //}
}

-(void)loadWatherInfo
{
    [DataManager instance].locationLong = currentLocation.coordinate.longitude;
    [DataManager instance].locationLat = currentLocation.coordinate.latitude;
    NSLog(@"lat %f and long %f", [DataManager instance].locationLong, [DataManager instance].locationLat);
    [[DataManager instance] loadWeather];
   
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)erro
{
    UIAlertView *alert;
    NSLog(@"CURR LOC %@", [erro description]);
    alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Can't retrieve current location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [self stop];
}

-(void)checkCurrentLocationData
{
    if([self locationKnown])
    {
        NSLog(@"location is %f %f", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
        
        [self stop];
    }
}

/* Next-Previous day */

-(IBAction)showNextDay:(id)sender
{
    [_scrollView setContentOffset:CGPointZero  animated:YES];
    nextDay = true; prevDay = false;
    //Animation:
    [UIView beginAnimations:@"ShowNewDay" context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.66];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
    [_hoursGridView setTransform:CGAffineTransformMakeScale(0.77,0.77)];
    CGRect f = _hoursGridView.frame;
    f.origin.y = 0;
    _hoursGridView.frame = f;
    [UIView commitAnimations];
    int next = [[AppData instance] getDay:YES];
    [calender goToPage:next flipUp:YES];
}

-(IBAction)showPreviousDay:(id)sender
{
    [_scrollView setContentOffset:CGPointZero animated:YES];
    prevDay = true; nextDay = false;
    //Animation:
    [UIView beginAnimations:@"ShowNewDay" context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.66];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
    
    [_hoursGridView setTransform:CGAffineTransformMakeScale(0.77,0.77)];
    CGRect f = _hoursGridView.frame;
    f.origin.y = 0;
    _hoursGridView.frame = f;
    [UIView commitAnimations];
    int prev = [[AppData instance] getDay:NO];
    [calender goToPage:prev flipUp:NO];
}

-(void)transitionDidStop:(NSString *)animationID finished:(BOOL)finished context:(void *)context
{
    
    if([animationID isEqualToString:@"ShowNewDay"])
    {
        [UIView beginAnimations:@"ShowNewDayOut" context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.44];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        //[_hoursGridView setTransform:CGAffineTransformMakeScale(0.77,0.01)];
        CGRect f = _hoursGridView.frame;
        f.origin.y = 0;
        _hoursGridView.frame = f;
        _hoursGridView.alpha = 0.1;
        [UIView commitAnimations];
    }else if([animationID isEqualToString:@"ShowNewDayOut"])
    {
        [self updateData:NO];
       
    }else if([animationID isEqualToString:@"ShowNewDayIn"])
    {
        
        CGRect f = _hoursGridView.frame;
        [UIView beginAnimations:@"ShowNewDayRestored" context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:.66];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        [_hoursGridView setTransform:CGAffineTransformMakeScale(1.0,1.0)];
        f = _hoursGridView.frame;
        f.origin.y = 0;
        _hoursGridView.frame = f;
        _hoursGridView.alpha = 1.0;
        [UIView commitAnimations];
    }else if([animationID isEqualToString:@"ShowDatePicker"]){
        
        
    }else if([animationID isEqualToString:@"HideDatePickerAndGoToDate"]){
        [_scrollView setContentOffset:CGPointZero animated:YES];
        //Animation:
        [UIView beginAnimations:@"ShowNewDayJump" context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.66];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        
        [_hoursGridView setTransform:CGAffineTransformMakeScale(0.77,0.77)];
        CGRect f = _hoursGridView.frame;
        f.origin.y = 0;
        _hoursGridView.frame = f;
        [UIView commitAnimations];
        
        
        
    }else if([animationID isEqualToString:@"ShowNewDayJump"])
    {
        dayBefore = [[AppData instance] currentDay];
        [UIView beginAnimations:@"ShowNewDayOutJump" context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.44];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        //[_hoursGridView setTransform:CGAffineTransformMakeScale(0.77,0.01)];
        CGRect f = _hoursGridView.frame;
        f.origin.y = 0;
        _hoursGridView.frame = f;
        _hoursGridView.alpha = 0.1;
        [UIView commitAnimations];
    }else if([animationID isEqualToString:@"ShowNewDayOutJump"])
    {
        [self updateData:YES];
        if(dayBefore < [[AppData instance] currentDay])
            [calender goToPage:[[AppData instance] currentDay] flipUp:YES];
        else
            [calender goToPage:[[AppData instance] currentDay] flipUp:NO];
        
    }else if([animationID isEqualToString:@"ShowNewDayInJump"])
    {
        
        CGRect f = _hoursGridView.frame;
        [UIView beginAnimations:@"ShowNewDayRestoredJump" context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:.66];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        [_hoursGridView setTransform:CGAffineTransformMakeScale(1.0,1.0)];
        f = _hoursGridView.frame;
        f.origin.y = 0;
        _hoursGridView.frame = f;
        _hoursGridView.alpha = 1.0;
        [UIView commitAnimations];
    }else if([animationID isEqualToString:@"ShowDayFreeTime"])
    {
        [[AppData instance] showFreeTimeForCurrentDate];
        [_hoursGridView updateGridView];
        //_hoursGridView.frame = CGRectMake(0,0,320,2200);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:.44];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        
        _hoursGridView.alpha = 1.0;
        [UIView commitAnimations];
    }
}

-(void)updateData:(BOOL)jumpTodate
{
    if(jumpTodate)
    {
        [[AppData instance] showJumpToDate];
    }else{
        if(nextDay)
        {
            [[AppData instance] nextDay];
            NSLog(@"next day");
        }else{
            [[AppData instance] prevDay];
            NSLog(@"prev day");
        }
        
    }
    _hoursGridView.transform=CGAffineTransformMakeScale(1.0,1.0);
    CGRect f = _hoursGridView.frame;
    f.origin.y = 0;
    _hoursGridView.frame = f;
    [_hoursGridView updateGridView];
    
    _hoursGridView.transform=CGAffineTransformMakeScale(0.77,0.77);
    f = _hoursGridView.frame;
    f.origin.y = 0;
    _hoursGridView.frame = f;
    
    //load the new day
    [UIView beginAnimations:@"ShowNewDayIn" context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.44];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
    [_hoursGridView setTransform:CGAffineTransformMakeScale(0.77,0.77)];
    f = _hoursGridView.frame;
    f.origin.y = 0;
    _hoursGridView.frame = f;
    _hoursGridView.alpha = 1.0;
    [UIView commitAnimations];
}

-(IBAction)showCalenderSelection:(id)sender
{
    NSLog(@"selecting a day");
    _dateView.frame = CGRectMake(0, dateViewYPos + _dateView.frame.size.height, _dateView.frame.size.width, _dateView.frame.size.height);
    _dateView.hidden = NO;
    //
    [UIView beginAnimations:@"ShowDatePicker" context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.44];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
    CGRect f = _dateView.frame;
    f.origin.y = dateViewYPos;
    _dateView.frame = f;
     
    [UIView commitAnimations];
    
   
}

-(IBAction)datePickerDone:(id)sender
{
    NSDate *date = _pickerView.date;
    [AppData instance].jumpToDate = date;
    
    [UIView beginAnimations:@"HideDatePickerAndGoToDate" context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.44];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
      _dateView.frame = CGRectMake(0, dateViewYPos + _dateView.frame.size.height, _dateView.frame.size.width, _dateView.frame.size.height);
    [UIView commitAnimations];
}

-(IBAction)datePickerCancel:(id)sender
{
    [UIView beginAnimations:@"HideDatePicker" context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.33];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
      _dateView.frame = CGRectMake(0, dateViewYPos + _dateView.frame.size.height, _dateView.frame.size.width, _dateView.frame.size.height);
    
    [UIView commitAnimations];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"picker selection");
}

-(IBAction)showOnlyFreeTime:(id)sender
{
    
    
    [_scrollView setContentOffset:CGPointZero animated:YES];
    
    //Animation:
    [UIView beginAnimations:@"ShowDayFreeTime" context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.66];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
    
    //[_hoursGridView setTransform:CGAffineTransformMakeScale(0.55,1.0)];
    CGRect f = _hoursGridView.frame;
    f.origin.y = 0;
    _hoursGridView.frame = f;
    _hoursGridView.alpha = 0.05;
    [UIView commitAnimations];
}

-(IBAction)howToBtnAction
{
    [self presentModalViewController:_howTo animated:YES];
}

-(void)setGridScrollViewSize:(NSNotification*)notif
{
    NSNumber *num = (NSNumber*)notif.object;
    _scrollView.contentSize = CGSizeMake(320, num.floatValue);
}


@end
