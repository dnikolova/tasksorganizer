//
//  TaskEntry.m
//  Tasks


#import "TaskEntry.h"
#import "LoadEmailDBAccessor.h"
#import "DateUtil.h"
#import "Task.h"
#import "AppData.h"

@implementation TaskEntry

static sqlite3_stmt *taskUpdateStmt = nil;
static sqlite3_stmt *taskInsertStmt = nil;

static TaskEntry *m_instance = nil;


+(TaskEntry*)instance
{
    if(m_instance == nil)
        m_instance = [[TaskEntry alloc] init];
    
    return m_instance;
}

+(void)createEmailSearchTable {
	sqlite3_stmt *testStmt = nil;
	// this "prepare statement" part is really just a method for checking if the tables exist yet
	NSString *updateStmt = @"SELECT docid FROM search_email WHERE subject = ?;";
	int dbrc = sqlite3_prepare_v2([[AddTaskDBAccessor sharedManager] database], [updateStmt UTF8String], -1, &testStmt, nil);	
	if (dbrc != SQLITE_OK) {
		// create index
		char* errorMsg;	
		NSString *statement = @"CREATE VIRTUAL TABLE search_email USING fts3(meta_string, subject, body);";
		//if([AppSettings dataInitVersion] != nil) { // new search_email format
			//statement = @"CREATE VIRTUAL TABLE search_email USING fts3(meta, subject, body, sender, tos, ccs, folder);";
		//}
		
		int res = sqlite3_exec([[AddTaskDBAccessor sharedManager] database],[[NSString stringWithString:statement] UTF8String] , NULL, NULL, &errorMsg);
		if (res != SQLITE_OK) {
			//NSString *errorMessage = [NSString stringWithFormat:@"Failed to create search_email with message '%s'.", errorMsg];
			//NSLog(@"errorMessage = '%@, original ERROR CODE = %i'",errorMessage,res);
		}
	} else {
		sqlite3_finalize(testStmt);
	}
}

/* Creating the main table */

+(void)verifyVirtualTables {
	// create tables as appropriate
	char* errorMsg;
    /*title, description, cateory, date, day,month,year,taskID, status,startHour,startMinutes,endHour,endMinutes,taskPriority*/
	int res = sqlite3_exec([[AddTaskDBAccessor sharedManager] database],[[NSString stringWithString:@"CREATE TABLE IF NOT EXISTS tasks "
																			  "(pk INTEGER PRIMARY KEY, datetime REAL, title TEXT, description TEXT, "
																			  "cateory INTEGER, day INTEGER, month INTEGER, year INTEGER,  task_id INTEGER,  status INTEGER,   start_hr INTEGER,   start_mins INTEGER,   end_hr INTEGER,   end_mins INTEGER, task_priority INTEGER, is_am INTEGER);"] UTF8String] , NULL, NULL, &errorMsg);
	if (res != SQLITE_OK) {
		NSString *errorMessage = [NSString stringWithFormat:@"Failed to create tasks table '%s'.", errorMsg];
		NSLog(@"errorMessage = '%@, original ERROR CODE = %i'",errorMessage,res);
	}else{
        NSLog(@"table tasks is successfully created");
    }
}

/* Loading all tasks from the database - use filter query below */

+ (NSArray *)tasksInfos {
    
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT * FROM tasks ORDER BY pk DESC";
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2([[AddTaskDBAccessor sharedManager] database], [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int uniqueId = sqlite3_column_int(statement, 0);
            char *titleChars = (char *) sqlite3_column_text(statement, 2);
            char *descChars = (char *) sqlite3_column_text(statement, 3);
            
            NSString *title = @"";
            if(titleChars)
                title = [[NSString alloc] initWithUTF8String:titleChars];
            NSString *desc = @"";
            if(descChars)
                desc = [[NSString alloc] initWithUTF8String:descChars];
            Task *task = [[Task alloc] init];
            task.taskID = uniqueId;
            task.title = title;
            task.description = desc;
            task.cateory = sqlite3_column_int(statement, 4);
            task.day = sqlite3_column_int(statement, 5);
            task.month = sqlite3_column_int(statement, 6);
            task.year = sqlite3_column_int(statement, 7);
            task.taskID = sqlite3_column_int(statement, 8);
            task.status = sqlite3_column_int(statement, 9);
            task.startHour = sqlite3_column_int(statement, 10);
            task.startMinutes = sqlite3_column_int(statement, 11);
            task.endHour = sqlite3_column_int(statement, 12);
            task.endMinutes = sqlite3_column_int(statement, 13);
            task.taskPriority = sqlite3_column_int(statement, 14);
            
            [retval addObject:task];
            
            NSLog(@"loaded task with name %@ desc %@ start_h %d day %d month %d year %d", task.title, task.description, task.startHour, task.day, task.month, task.year);
            
        }
        sqlite3_finalize(statement);
    }
    
    NSLog(@"loaded %d task objects from the Virt. db", retval.count);
    
    return retval;
    
}

/* Using the method to load all tasks for a specified date */

+(void)selectTasksForDay:(int)day month:(int)month year:(int)year
{
    //select -> create objects -> push in an array in AppData for currently displayed day
    static sqlite3_stmt *taskLoadStmt = nil;
	
	NSString *statement = [NSString stringWithFormat:@"SELECT * FROM tasks WHERE day = %i AND month = %i AND year = %i;", day, month, year];
    
	int dbrc;
	dbrc = sqlite3_prepare_v2([[AddTaskDBAccessor sharedManager] database], [statement UTF8String], -1, &taskLoadStmt, nil);
	if (dbrc != SQLITE_OK) {
        NSLog(@"Failed step in loadData with error %s", sqlite3_errmsg([[AddTaskDBAccessor sharedManager] database]));
	}
	
	NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss.SSSS"];
	 
    NSMutableArray *results = [[NSMutableArray alloc] init];
	while (sqlite3_step(taskLoadStmt) == SQLITE_ROW) {
        
        Task *taskItem = [[Task alloc] init];
		taskItem.taskID = sqlite3_column_int(taskLoadStmt, 0);
		
		NSDate *date = [NSDate date]; // default == now!
		const char * sqlVal = (const char *)sqlite3_column_text(taskLoadStmt, 1);
		if(sqlVal != nil) {
			NSString *dateString = [NSString stringWithUTF8String:sqlVal];
			date = [DateUtil datetimeInLocal:[dateFormatter dateFromString:dateString]];
            taskItem.date = dateString;
		}
		
		/*
         0 pk INTEGER PRIMARY KEY,
         1 datetime REAL,
         2 title TEXT,
         3 description TEXT, "
         */
		NSString* title = @"";
		sqlVal = (const char *)sqlite3_column_text(taskLoadStmt, 2);
		if(sqlVal != nil) {
            title = [NSString stringWithUTF8String:sqlVal];
        }
		taskItem.title = title;
		
        NSString* desc = @"";
		sqlVal = (const char *)sqlite3_column_text(taskLoadStmt, 3);
		if(sqlVal != nil) {	desc = [NSString stringWithUTF8String:sqlVal]; }
		taskItem.description = desc;
		
        /*4 cateory INTEGER,
         5 day INTEGER,
         6 month INTEGER,
         7 year INTEGER,
         8 task_id INTEGER,
         9 status INTEGER,
         10 start_hr INTEGER,
         11 start_mins INTEGER,
         12 end_hr INTEGER,
         13 end_mins INTEGER,
         14 task_priority INTEGER
         */
        
        taskItem.cateory = sqlite3_column_int(taskLoadStmt, 4);
		taskItem.day = sqlite3_column_int(taskLoadStmt, 5);
        taskItem.month = sqlite3_column_int(taskLoadStmt, 6);
        taskItem.year = sqlite3_column_int(taskLoadStmt, 7);
        taskItem.taskID = sqlite3_column_int(taskLoadStmt, 8);
        taskItem.status = sqlite3_column_int(taskLoadStmt, 9);
        taskItem.startHour = sqlite3_column_int(taskLoadStmt, 10);
        taskItem.startMinutes = sqlite3_column_int(taskLoadStmt, 11);
        taskItem.endHour = sqlite3_column_int(taskLoadStmt, 12);
        taskItem.endMinutes = sqlite3_column_int(taskLoadStmt, 13);
        taskItem.taskPriority = sqlite3_column_int(taskLoadStmt, 14);
		
        [results addObject:taskItem];
        
	}
	
	sqlite3_finalize(taskLoadStmt);
    
    NSLog(@"loaded db tasks for %d %d %d", results.count, day, month);
    [AppData instance].dbTasks = [[NSMutableArray alloc] init];
    [AppData instance].dbTasks = results;
    
    //post notification for table reload
}


/* Adding a new task in the database */

+(int)addTask:(Task*)task
{
    //Insert st-t
    if(taskInsertStmt == nil) {
		NSString *updateContact = @"INSERT INTO tasks(title, description, cateory, day,month,year,task_id,status,start_hr,start_mins,end_hr,end_mins,task_priority) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
		int dbrc = sqlite3_prepare_v2([[AddTaskDBAccessor sharedManager] database], [updateContact UTF8String], -1, &taskInsertStmt, nil);
		if (dbrc != SQLITE_OK) {
			return 0;
		}
	}
	
	sqlite3_bind_text(taskInsertStmt, 1, [task.title cStringUsingEncoding:NSASCIIStringEncoding], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(taskInsertStmt, 2, [task.description cStringUsingEncoding:NSASCIIStringEncoding], -1, SQLITE_TRANSIENT);
    
    /*4 cateory INTEGER,
     5 day INTEGER,
     6 month INTEGER,
     7 year INTEGER,
     8 task_id INTEGER,
     9 status INTEGER,
     10 start_hr INTEGER,
     11 start_mins INTEGER,
     12 end_hr INTEGER,
     13 end_mins INTEGER,
     14 task_priority INTEGER
     */
	sqlite3_bind_int(taskInsertStmt, 3, task.cateory);
    sqlite3_bind_int(taskInsertStmt, 4, task.day);
    sqlite3_bind_int(taskInsertStmt, 5, task.month);
    sqlite3_bind_int(taskInsertStmt, 6, task.year);
    sqlite3_bind_int(taskInsertStmt, 7, task.taskID);
    sqlite3_bind_int(taskInsertStmt, 8, task.status);
    sqlite3_bind_int(taskInsertStmt, 9, task.startHour);
    sqlite3_bind_int(taskInsertStmt, 10, task.startMinutes);
    sqlite3_bind_int(taskInsertStmt, 11, task.endHour);
    sqlite3_bind_int(taskInsertStmt, 12, task.endMinutes);
    sqlite3_bind_int(taskInsertStmt, 13, task.taskPriority);
	
	if (sqlite3_step(taskInsertStmt) != SQLITE_DONE)	{
		NSLog(@"==========> Error inserting task with name %@", task.title);
	}else{
        NSLog(@"Task with for day/month/year %d/%d/%d added end time: %d %d", task.day, task.month,task.year, task.endHour, task.endMinutes);
    }
	sqlite3_reset(taskInsertStmt);
	
	return (int)sqlite3_last_insert_rowid([[AddTaskDBAccessor sharedManager] database]);
}

/* Updating a single entry */

+(void)updateTask:(Task*)task
{
    NSLog(@"updating the tasks status %d", task.status);
    
    if(taskUpdateStmt==nil){
        const char *sql = "UPDATE tasks SET title = ? , description = ? , task_priority = ? , status = ? WHERE day =? AND month = ?  AND year = ?  AND start_hr = ? AND  start_mins = ?";
        if(sqlite3_prepare_v2([[AddTaskDBAccessor sharedManager] database], sql, -1, &taskUpdateStmt, NULL) !=SQLITE_OK)
        {
            NSLog(@"err");
        }
        
    }
    sqlite3_bind_text(taskUpdateStmt, 1, [task.title UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(taskUpdateStmt, 2, [task.description UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(taskUpdateStmt, 3, task.taskPriority);
    sqlite3_bind_int(taskUpdateStmt, 4, task.status);
    sqlite3_bind_int(taskUpdateStmt, 5, task.day);
    sqlite3_bind_int(taskUpdateStmt, 6, task.month);
    sqlite3_bind_int(taskUpdateStmt, 7, task.year);
    sqlite3_bind_int(taskUpdateStmt, 8, task.startHour);
    sqlite3_bind_int(taskUpdateStmt, 9, task.startMinutes);
    
    if (SQLITE_DONE != sqlite3_step(taskUpdateStmt))
    {
        NSLog(@"err updating");
    }
    
    
    sqlite3_reset(taskUpdateStmt);
    return;
    char* errorMsg;
	int res = sqlite3_exec([[AddTaskDBAccessor sharedManager] database],[[NSString stringWithFormat:@"UPDATE tasks SET title=%@ ,  description=%@ , status=%i , task_priority=%i WHERE day=%i AND month=%i AND year=%i AND start_hr=%i AND start_mins=%i;", task.title, task.description,task.status,task.taskPriority, task.day,task.month,task.year,task.startHour, task.startMinutes] UTF8String] , NULL, NULL, &errorMsg);
	if (res != SQLITE_OK) {
		NSString *errorMessage = [NSString stringWithFormat:@"Failed to update task with message '%s'.", errorMsg];
		NSLog(@"errorMessage = '%@, original ERROR CODE = %i'",errorMessage,res);
	}
    return;
	if(taskUpdateStmt == nil) {
        ;
        //TO DO - add all properties in the update st-t:
        NSString *updateTask = [NSString stringWithFormat:@"UPDATE tasks SET title=%@, description=%@ status=%i task_priority=%i, WHERE pk=%i;", task.title, task.description,task.status,task.taskPriority, task.taskID];
        NSLog(@"update stmnt %@", updateTask);
		//NSString *updateTask = @"UPDATE tasks SET title=?, description=?, WHERE pk = ?;";
		int dbrc = sqlite3_prepare_v2([[AddTaskDBAccessor sharedManager] database], [updateTask UTF8String], -1, &taskUpdateStmt, nil);
		if (dbrc != SQLITE_OK) {
            NSLog(@"update failed for entry %d", task.taskID);
			return;
		}
	}
	
	sqlite3_bind_text(taskUpdateStmt, 1, [task.title UTF8String], -1, SQLITE_TRANSIENT);
	//sqlite3_bind_int(taskUpdateStmt, 2, 0);
	
	if (sqlite3_step(taskUpdateStmt) != SQLITE_DONE) {
		NSLog(@"==========> Error updating task with ID %i", task.taskID);
	}else{
        NSLog(@"==========> Success updating task with ID %i", task.taskID);
    }
	sqlite3_reset(taskUpdateStmt);

}

+(void)deleteTask:(Task*)task
{
    char* errorMsg;
	int res = sqlite3_exec([[AddTaskDBAccessor sharedManager] database],[[NSString stringWithFormat:@"DELETE FROM tasks WHERE start_hr=%i AND start_mins = %i AND year = %i AND month = %i AND day = %i;", task.startHour, task.startMinutes, task.year, task.month, task.day] UTF8String] , NULL, NULL, &errorMsg);
	if (res != SQLITE_OK) {
		NSString *errorMessage = [NSString stringWithFormat:@"Failed to delete task with message '%s'.", errorMsg];
		NSLog(@"errorMessage = '%@, original ERROR CODE = %i'",errorMessage,res);
	}
}

/* Loading a single task using its ID */

-(void)loadTask:(int)pkToLoad
{
	static sqlite3_stmt *taskLoadStmt = nil;
	
	NSString *statement = [NSString stringWithFormat:@"SELECT * FROM tasks WHERE pk = %i LIMIT 1;", pkToLoad];
   
	int dbrc;
	dbrc = sqlite3_prepare_v2([[AddTaskDBAccessor sharedManager] database], [statement UTF8String], -1, &taskLoadStmt, nil);
	if (dbrc != SQLITE_OK) {
			NSLog(@"Failed step in loadData with error %s", sqlite3_errmsg([[AddTaskDBAccessor sharedManager] database]));
	}		
	
	NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init]; 
	[dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss.SSSS"];
	
    Task *taskItem = [[Task alloc] init];
    
	//Exec query - TO DO - edit properties below
	if(sqlite3_step (taskLoadStmt) == SQLITE_ROW) {
        
        
		taskItem.taskID = sqlite3_column_int(taskLoadStmt, 0);
		
		NSDate *date = [NSDate date]; // default == now!
		const char * sqlVal = (const char *)sqlite3_column_text(taskLoadStmt, 1);
		if(sqlVal != nil) {
			NSString *dateString = [NSString stringWithUTF8String:sqlVal];
			date = [DateUtil datetimeInLocal:[dateFormatter dateFromString:dateString]];
            taskItem.date = dateString;
		}
		
		/*
         0 pk INTEGER PRIMARY KEY,
         1 datetime REAL,
         2 title TEXT,
         3 description TEXT, "
         */
		NSString* title = @"";
		sqlVal = (const char *)sqlite3_column_text(taskLoadStmt, 2);
		if(sqlVal != nil) {
            title = [NSString stringWithUTF8String:sqlVal];
        }
		taskItem.title = title;
		
        NSString* desc = @"";
		sqlVal = (const char *)sqlite3_column_text(taskLoadStmt, 3);
		if(sqlVal != nil) {	desc = [NSString stringWithUTF8String:sqlVal]; }
		taskItem.description = desc;
		
        /*4 cateory INTEGER,
         5 day INTEGER,
         6 month INTEGER,
         7 year INTEGER,
         8 task_id INTEGER,
         9 status INTEGER,
         10 start_hr INTEGER,
         11 start_mins INTEGER,
         12 end_hr INTEGER,
         13 end_mins INTEGER,
         14 task_priority INTEGER
         */
        
        taskItem.cateory = sqlite3_column_int(taskLoadStmt, 4);
		taskItem.day = sqlite3_column_int(taskLoadStmt, 5);
        taskItem.month = sqlite3_column_int(taskLoadStmt, 6);
        taskItem.year = sqlite3_column_int(taskLoadStmt, 7);
        taskItem.taskID = sqlite3_column_int(taskLoadStmt, 8);
        taskItem.status = sqlite3_column_int(taskLoadStmt, 9);
        taskItem.startHour = sqlite3_column_int(taskLoadStmt, 10);
        taskItem.startMinutes = sqlite3_column_int(taskLoadStmt, 11);
        taskItem.endHour = sqlite3_column_int(taskLoadStmt, 12);
        taskItem.endMinutes = sqlite3_column_int(taskLoadStmt, 12);
        taskItem.taskPriority = sqlite3_column_int(taskLoadStmt, 14);
		
        [AppData instance].targetTask = taskItem;

	} 
	
	sqlite3_finalize(taskLoadStmt);	
	
}

@end

