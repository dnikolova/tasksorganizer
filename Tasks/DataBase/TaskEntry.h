//
//  TaskEntry.h
//  Tasks
//

#import <Foundation/Foundation.h>	
#import "sqlite3.h"

#import "AddTaskDBAccessor.h" // for SQLite3 constants

#import <objc/runtime.h>
#import <objc/message.h>

@class Task;

@interface TaskEntry : NSObject {
	
    
@private
	sqlite3 *database;
}


+(TaskEntry*)instance;
+(void)verifyVirtualTables;
+(NSArray *)tasksInfos;
+(void)selectTasksForDay:(int)day month:(int)month year:(int)year;
+(void)updateTask:(Task*)task;
+(void)deleteTask:(Task*)task;
+(int)addTask:(Task*)task;//returns the pk
-(void)loadTask:(int)pkToLoad;

@end




