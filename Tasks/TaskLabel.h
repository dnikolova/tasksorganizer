//
//  TaskLabel.h
//  Tasks
//
//  Created by Desislava Nikolova on 3/4/13.
//  Copyright (c) 2013 DesiNikolova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskLabel : UILabel

@property(nonatomic,retain) UIColor *fill;
@property(nonatomic,retain) UIColor *stroke;
@property(nonatomic,retain) UIColor *drawingTextColor;
@property(nonatomic,retain) NSString *drawingTextString;
@property(nonatomic, assign) CGFloat cornerRadius;
@property(nonatomic, assign) BOOL drawsBorder, textAlignmentCenter;
@property(nonatomic, assign) CGFloat fontSize;
@property(nonatomic, assign) BOOL boldFont;
@property(nonatomic, assign) BOOL completed;

@end
