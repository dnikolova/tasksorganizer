//
//  WeatherInfoView.m
//  Tasks
//
//  Created by Desislava Nikolova on 1/12/13.
//  Copyright (c) 2013 DesiNikolova. All rights reserved.
//

#import "WeatherInfoView.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "Constants.h"
#import "DataManager.h"


@implementation WeatherInfoView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        isInfoRotating = false;
        displayingWeatherInfo = true;
    }
    return self;
}

/*
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    isInfoRotating = !isInfoRotating;
    if(isInfoRotating){
        //[self startRotatingInfo];
    }else{
       // [_timer invalidate];
       // _timer = nil;
    }
    
}
*/
-(void)startRotatingInfo
{
    //disabled for now;
    return;
    isInfoRotating = true;
    displayingWeatherInfo = true;

    _timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(rotatingInfoViews) userInfo:nil repeats:YES];
}

-(void)rotatingInfoViews
{
    //NSLog(@"debug %d",displayingWeatherInfo);
    if(displayingWeatherInfo == false)
    {
        //NSLog(@"will display the weather info!");
        displayingWeatherInfo = true;
        CGRect _frame = _wInfoView.frame;
        _frame.origin.y = _wInfoView.frame.size.height;
        _wInfoView.frame = _frame;
        [_wInfoView setNeedsDisplay];
        
        _frame.origin.y = 0;
        
        CGRect _visible_frame = _locInfoView.frame;
        _visible_frame.origin.y -= _visible_frame.size.height;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.44];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        //[UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        
        _wInfoView.frame = _frame;
        _locInfoView.frame = _visible_frame;
        
        
        [UIView commitAnimations];
        
        return;
        
    }else{
       // NSLog(@"will display the location info!");
        displayingWeatherInfo = false;
        CGRect _frame = _locInfoView.frame;
        _frame.origin.y = _locInfoView.frame.size.height;
        _locInfoView.frame = _frame;
        [_locInfoView setNeedsDisplay];
        
        _frame.origin.y = 0;
        
        CGRect _visible_frame = _wInfoView.frame;
        _visible_frame.origin.y = -_visible_frame.size.height;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.44];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        //[UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        
        _locInfoView.frame = _frame;
        _wInfoView.frame = _visible_frame;
        
        
        [UIView commitAnimations];
    }
    
}

+(void)transitionDidStop:(NSString *)animationID finished:(BOOL)finished context:(void *)context
{
   //
    NSLog(@"animation done!");
    
     
}

/*Weather */

-(void)updateWeather
{
    NSLog(@"updating the weather");
    if([[DataManager instance].weatherInfo objectForKey:@"code"] == nil)
    {
       // _locInfoView.hidden = YES;
       // _wInfoView.hidden = YES;
        _noData.hidden = NO;
        _degrees.text = @"";
        _condistions.text = @"";
        return;
    }
    
    _locInfoView.hidden = NO;
    _wInfoView.hidden = NO;
    _noData.hidden = YES;
    
    int code = [[[DataManager instance].weatherInfo objectForKey:@"code"] intValue];
    NSMutableArray *icons = [[DataManager instance] weatherIconForCode:code ];
    if(icons.count > 0)
        _weatherIcon1.image = [UIImage imageNamed:[icons objectAtIndex:0]];
    if(icons.count > 1)
        _weatherIcon2.image = [UIImage imageNamed:[icons objectAtIndex:1]];
    _degrees.text  = [NSString stringWithFormat:@"%@ %@ F",[[DataManager instance].weatherInfo objectForKey:@"temp"], @"\u00B0"];
    _condistions.text =[[DataManager instance].weatherInfo objectForKey:@"text"];
    NSString *time = [[DataManager instance].weatherInfo objectForKey:@"date"] ;
    NSString *hour = [time substringFromIndex:17];
    _locationAndTime.text = [NSString stringWithFormat:@" %@ %@, %@", [[DataManager instance].loacationInfo objectForKey:@"city"], [[DataManager instance].loacationInfo objectForKey:@"state"], hour  ];
    
    self.hidden = NO;
    [self startRotatingInfo];
    
}

-(IBAction)weatherMore:(id)sender
{
    //animate from the right
    AppDelegate *_delegate = [[UIApplication sharedApplication] delegate];
    [[_delegate rootViewController] showMoreWeather];
    
}

-(IBAction)reloadWeather:(id)sender
{
    [DataManager instance].woeidLoaded = false;
    [[DataManager instance] getWeatherWoeid];
}


-(IBAction)setLocation:(id)sender
{
    
}



@end
