//
//  ViewUtils.m
//  Tasks
//
//  Created by Desislava  Nikolova on 9/16/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import "ViewUtils.h"

@implementation ViewUtils

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+(void)animateViewIn:(UIView*)view fromDirection:(BOOL)top
{
    CGRect _frame = view.frame;
    float _old_origin = _frame.origin.y;
    _frame.origin.y = -_frame.size.height;
    view.frame = _frame;
    view.alpha = 0.0;
    
    _frame.origin.y  = _old_origin;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.55];
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
    
    view.frame = _frame;
    view.alpha = 1.0;
    
    [UIView commitAnimations];
}

+(void)transitionDidStop:(NSString *)animationID finished:(BOOL)finished context:(void *)context
{
    NSLog(@"done");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TasksViewDisplayed" object:nil];
}

+(void)animateViewFromLeft:(UIView*)view
{
    view.transform = CGAffineTransformScale(view.transform, 0.0, 0.0);
    [UIView animateWithDuration:0.8 delay:0.0 options:0
                     animations:^{
                         view.transform = CGAffineTransformScale(view.transform, 1.0, 1.0);
                     }
                     completion:nil];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
