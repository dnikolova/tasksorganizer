//
//  ViewUtils.h
//  Tasks
//
//  Created by Desislava  Nikolova on 9/16/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewUtils : UIView

+(void)animateViewIn:(UIView*)view fromDirection:(BOOL)top;
+(void)animateViewFromLeft:(UIView*)view;

+(void)transitionDidStop:(NSString *)animationID finished:(BOOL)finished context:(void *)context;

@end
