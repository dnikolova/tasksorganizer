//
//  AppDelegate.h
//  Tasks
//
//  Created by Desislava  Nikolova on 9/15/12.
//  Copyright (c) 2012 ScopicSoftware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,retain)ViewController *rootViewController;
@end
