//
//  AppData.m
//  Tasks
//
//  Created by Desislava Nikolova on 3/3/13.
//  Copyright (c) 2013 DesiNikolova. All rights reserved.
//

#import "AppData.h"
#import "TaskEntry.h"

@implementation AppData

@synthesize workingTask,targetTask, tasks, dbTasks, jumpToDate,themeColor,weatherOn,topicsArray,topicsNames;

static AppData *m_instance = nil;


+(AppData*)instance
{
    if(!m_instance)
    {
        m_instance = [[AppData alloc] init];
    }
    return m_instance;
}

- (id)init
{
    self = [super init];
    self.tasks = [[NSMutableArray alloc] init];
    _tasksIDs = 0; // or the highest from the virual database
    
    NSCalendar *calender = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDate *date = [NSDate date];
    NSDateComponents *comps = [calender components:unitFlags fromDate:date];
    day = comps.day;
    month = comps.month;
    year = comps.year;
    
    themeColor = 3;
    weatherOn = true;
    
    topicsArray = [[NSMutableArray alloc] initWithObjects: @"brief_case.png",@"piggy_bank.png",@"people_family.png", @"heart.png",@"house.png",@"pacman.png",@"lunch.png", @"working.png",nil];
    
    topicsNames = [[NSMutableArray alloc] initWithObjects:@"Travel", @"At the Bank", @"With Family", @"Dating",@"At Home", @"Happy Hour", @"Eating", @"Working", nil];
    
    return self;
}

/* Feeding the tasks for the day */

-(void)setUpDailyHoursGrid
{
    tasks = [[NSMutableArray alloc] init];
    bool frac = false;
    int maxHours = 0;
    
    while (maxHours < 48)
    {
       
        int hour = (int)(maxHours/2) + 6;
        int mins = (frac) ? 30 : 0;
        
        BOOL savedTask = [self checkSavedTasks:hour mins:mins andAddToDisplay:YES];
        if(savedTask)
        {
            Task *t = dbTask;
            frac = (t.endMinutes == 30) ? true : false;
            
            float task_start = t.startHour + ((t.startMinutes > 0) ? .5 : 0);
            float task_end = t.endHour + ((t.endMinutes > 0) ? .5 : 0);
            maxHours += (int)((task_end - task_start ) / .5);
            //NSLog(@"-- %f -- %f --%d",task_end - task_start,(task_end - task_start ) / .5,maxHours);
            //frac = !frac;
            continue;
        }
        //
        
        if(hour > 24)hour -= 24;
        NSString *a = @"AM";
        if(hour > 12)a = @"PM";
        NSString *title = [NSString stringWithFormat:@"Tap to add task at %d:00 %@", hour, a];
        if(frac)
            title = [NSString stringWithFormat:@"Tap to add task at %d:30 %@", hour, a];
        
        
        //Create empty tasks:
        Task *newEmptyTask = [[Task alloc] init];
        newEmptyTask.day = day;
        newEmptyTask.month = month;
        newEmptyTask.year = year;
        newEmptyTask.startHour = hour;
        newEmptyTask.startMinutes = (frac) ? 30 : 0;
        newEmptyTask.buttonTitle = [NSString stringWithString:title];
        
        [self saveEntryTask:newEmptyTask];
        
        maxHours++;
        
        frac = !frac;
        
        
    }
}

-(void)availableTimeSlots
{
    tasks = [[NSMutableArray alloc] init];
    bool frac = false;
    int maxHours = 0;
    while (maxHours < 48)
    {
        
        int hour = (int)(maxHours/2) + 6;
        int mins = (frac) ? 30 : 0;
        
        BOOL savedTask = [self checkSavedTasks:hour mins:mins andAddToDisplay:NO];
        if(savedTask)
        {
            Task *t = dbTask;
            frac = (t.endMinutes == 30) ? true : false;
            
            float task_start = t.startHour + ((t.startMinutes > 0) ? .5 : 0);
            float task_end = t.endHour + ((t.endMinutes > 0) ? .5 : 0);
            maxHours += (int)((task_end - task_start ) / .5);
            //NSLog(@"-- %f -- %f --%d",task_end - task_start,(task_end - task_start ) / .5,maxHours);
            //frac = !frac;
            continue;
        }
        //
        
        if(hour > 24)hour -= 24;
        NSString *a = @"AM";
        if(hour > 12)a = @"PM";
        NSString *title = [NSString stringWithFormat:@"Tap to add task at %d:00 %@", hour, a];
        if(frac)
            title = [NSString stringWithFormat:@"Tap to add task at %d:30 %@", hour, a];
        
        
        //Create empty tasks:
        Task *newEmptyTask = [[Task alloc] init];
        newEmptyTask.day = day;
        newEmptyTask.month = month;
        newEmptyTask.year = year;
        newEmptyTask.startHour = hour;
        newEmptyTask.startMinutes = (frac) ? 30 : 0;
        newEmptyTask.buttonTitle = [NSString stringWithString:title];
        
        [self saveEntryTask:newEmptyTask];
        
        maxHours++;
        
        frac = !frac;
        
        
    }
}

-(BOOL)checkSavedTasks:(int)hour mins:(int)mins andAddToDisplay:(BOOL)add
{
    for(int k = 0; k < dbTasks.count; k++)
    {
        
        Task *t = [dbTasks objectAtIndex:k];
        
        //NSLog(@"db task %d, st hr %d", dbTasks.count, t.startHour);
        if(t.startHour == hour && t.startMinutes == mins && !t.displayed)
        {
            dbTask = t;
            t.buttonTitle = [NSString stringWithString:t.title];
            t.edited = YES;
            t.displayed = YES;
            if(add)
                [self saveEntryTask:t];
            
            return YES;
        }
    }
    
    return NO;
}

-(void)retrieveTasks
{
    [TaskEntry verifyVirtualTables];
    
    [self retrieveTasksForDay:day month:month year:year];
    
}

-(void)retrieveTasksForDay:(int)t_day month:(int)t_month year:(int)t_year
{
    [TaskEntry verifyVirtualTables];
    //tasks = [[NSMutableArray alloc] init];
    //dbTasks =  [[NSMutableArray alloc] init];
    [TaskEntry selectTasksForDay:t_day month:t_month year:t_year];
    [self setUpDailyHoursGrid];
    NSLog(@"loading day month year %d %d %d %d", day,month, year, dbTasks.count);
    /*
    for(int i = 0; i < dbTasks.count; i++)
    {
        Task *t = [dbTasks objectAtIndex:i];
        NSLog(@"DB -- t st -- end : %d %d %d %d", t.startHour, t.startMinutes, t.endHour, t.endMinutes);
    }
     */
}

-(void)nextDay
{
    
    NSCalendar *calender = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    
    NSDateComponents* comps_curr = [[NSDateComponents alloc] init];
    comps_curr.day = day; comps_curr.month = month; comps_curr.year = year;
    NSDate *currentDate = [calender dateFromComponents:comps_curr];
    NSDateComponents* components = [[NSDateComponents alloc] init];
    components.day = 1;
    NSDate* newDate = [calender dateByAddingComponents: components toDate: currentDate options: 0];
    NSDateComponents *comps = [calender components:unitFlags fromDate:newDate];
    
    day = comps.day; month = comps.month; year = comps.year;
    NSLog(@"retr date %d %d %d", day, month,year);
    [TaskEntry selectTasksForDay:day month:month year:year];
    
    [self setUpDailyHoursGrid];
}

-(void)prevDay
{
    NSCalendar *calender = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    
    NSDateComponents* comps_curr = [[NSDateComponents alloc] init];
    comps_curr.day = day; comps_curr.month = month; comps_curr.year = year;
    NSDate *currentDate = [calender dateFromComponents:comps_curr];
    NSDateComponents* components = [[NSDateComponents alloc] init];
    components.day = -1;
    NSDate* newDate = [calender dateByAddingComponents: components toDate: currentDate options: 0];
    NSDateComponents *comps = [calender components:unitFlags fromDate:newDate];
    
    day = comps.day; month = comps.month; year = comps.year;
    NSLog(@"retr date %d %d %d %@ %@", day, month,year, [currentDate description], [newDate description]);
    [TaskEntry selectTasksForDay:day month:month year:year];
    [self setUpDailyHoursGrid];
}

-(void)showFreeTimeForCurrentDate
{
    NSCalendar *calender = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    
    NSDateComponents *comps = [calender components:unitFlags fromDate:jumpToDate];
    
    day = comps.day; month = comps.month; year = comps.year;
    
    [TaskEntry selectTasksForDay:day month:month year:year];
    [self availableTimeSlots];
}

-(void)showJumpToDate
{
    NSCalendar *calender = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    
    NSDateComponents *comps = [calender components:unitFlags fromDate:jumpToDate];
    
    day = comps.day; month = comps.month; year = comps.year;
    
    [TaskEntry selectTasksForDay:day month:month year:year];
    [self setUpDailyHoursGrid];
}

-(void)saveEntryTask:(Task*)task
{
    if(!task.edited)
    {
        _tasksIDs ++;
        task.taskID = _tasksIDs;
    }
    
    [tasks addObject:task];
}

-(void)saveTask
{
    Task *t = [workingTask copyTaskObject];
    _tasksIDs ++;
    t.taskID = _tasksIDs;
    
    NSCalendar *calender = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDate *date = [NSDate date];
    NSDateComponents *comps = [calender components:unitFlags fromDate:date];
    t.day = comps.day;
    t.month = comps.month;
    t.year = comps.year;
    
    [tasks addObject:t];
    
    NSLog(@"new task with id %d ; num tasks %d", t.taskID, tasks.count);
}

-(void)updateTask
{
    NSCalendar *calender = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDate *date = [NSDate date];
    NSDateComponents *comps = [calender components:unitFlags fromDate:date];
    workingTask.day = day;
    workingTask.month = month;
    workingTask.year = year;
    int res = [TaskEntry addTask:self.workingTask];
    self.workingTask.taskID = res;
    
    NSLog(@"Updated/Added a task with ID %d", res);
    
    [TaskEntry selectTasksForDay:day month:month year:year];
    
    [self setUpDailyHoursGrid];
}

-(void)saveTaskChanges
{
    [TaskEntry updateTask:self.workingTask];
}

-(void)deleteTask:(Task*)task
{
    for(int k = 0; k < tasks.count; k++)
    {
        
        Task *t = [tasks objectAtIndex:k];
        if(t.startHour == task.startHour && t.startMinutes == task.startMinutes)
        {
            NSLog(@"deleting task with iD %d", t.taskID);
            if(t.endHour - t.startHour >= 1 && t.endMinutes != t.startMinutes )
            {
                //add missing slots
                int endHour = (t.startMinutes == 30) ? t.startHour + 1 : t.startHour;
                int endMinutes = (t.startMinutes == 30) ? 0 : 30;
                while(endHour <= t.endHour)
                {
                    Task *newTask = [[Task alloc] init];
                    newTask.startHour = endHour;
                    newTask.startMinutes = endMinutes;
                    //[tasks insertObject:newTask atIndex:k];
                    if(newTask.startMinutes == 30)
                    {
                        endHour++;
                        endMinutes = 0;
                    }else{
                        endMinutes = 30;
                    }
                }
            }
            [TaskEntry deleteTask:t];
            [t reset];
            //assign the topmost ID in the stack;
            break;
        }
    }
    
    [TaskEntry selectTasksForDay:day month:month year:year];
    
    [self setUpDailyHoursGrid];
}

-(BOOL)isTimeAvailable:(int)hour minutes:(int)minutes date:(NSString*)date
{
    return YES;
}

-(int)getDay:(BOOL)next
{
    NSCalendar *calender = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    
    NSDateComponents* comps_curr = [[NSDateComponents alloc] init];
    comps_curr.day = day; comps_curr.month = month; comps_curr.year = year;
    NSDate *currentDate = [calender dateFromComponents:comps_curr];
    NSDateComponents* components = [[NSDateComponents alloc] init];
    components.day = 1;
    if(!next)
    {
        components.day = -1;
    }
    NSDate* newDate = [calender dateByAddingComponents: components toDate: currentDate options: 0];
    NSDateComponents *comps = [calender components:unitFlags fromDate:newDate];
    
    return comps.day;
    
}

-(int)currentDay{return day;}

@end
