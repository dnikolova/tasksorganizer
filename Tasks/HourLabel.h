//
//  HourLabel.h
//  Tasks
//
//  Created by Desislava  Nikolova on 9/30/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Task.h"
#import "TaskLabel.h"
#import "Constants.h"


@interface HourLabel : UIButton
{
    UIImageView *taskTopicImg;
    UILabel *_title;
    TaskLabel *_duration;
    TaskLabel *_status;
    UITextView *_description;
    UIImageView *_priority;
    UIButton *_delete, *_edit;
    NSMutableArray *topicsArray;
}

@property (nonatomic, retain) Task *source;
@property (nonatomic, assign) int hour;
@property (nonatomic, assign) int minute;

-(void)config;

@end
