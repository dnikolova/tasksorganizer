//
//  SettingsViewController.m
//  Tasks
//
//  Created by Desislava Nikolova on 11/30/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import "SettingsViewController.h"
#import "AppData.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        themeSelection = 3;
        weatherOn = true;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
     [theme1 setImage:[UIImage imageNamed:@"checkmarkSelected.png"] forState:UIControlStateSelected];
     [theme2 setImage:[UIImage imageNamed:@"checkmarkSelected.png"] forState:UIControlStateSelected];
    [theme3 setImage:[UIImage imageNamed:@"checkmarkSelected.png"] forState:UIControlStateSelected];
    [theme4 setImage:[UIImage imageNamed:@"checkmarkSelected.png"] forState:UIControlStateSelected];
    theme3.selected = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)saveTheme
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveTheme" object:nil];
    
    [AppData instance].themeColor = themeSelection;
    [AppData instance].weatherOn = weatherOn;
    
}

-(IBAction)cancelTheme
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CancelTheme" object:nil];
}

-(IBAction)theme1BtnAction:(id)sender
{
    themeSelection = 1;
    theme1.selected = YES;
    theme2.selected = NO;
    theme3.selected = NO;
    theme4.selected = NO;
}

-(IBAction)theme2BtnAction:(id)sender
{
    themeSelection = 2;
    theme1.selected = NO;
    theme2.selected = YES;
    theme3.selected = NO;
    theme4.selected = NO;
}

-(IBAction)theme3BtnAction:(id)sender
{
    themeSelection = 3;
    theme1.selected = NO;
    theme2.selected = NO;
    theme3.selected = YES;
    theme4.selected = NO;
}

-(IBAction)theme4BtnAction:(id)sender
{
    themeSelection = 4;
    theme1.selected = NO;
    theme2.selected = NO;
    theme3.selected = NO;
    theme4.selected = YES;
}

-(IBAction)weatherOn:(UISegmentedControl*)sender
{
    NSLog(@"%d",weatherOnOff.selectedSegmentIndex);
    weatherOn = weatherOnOff.selectedSegmentIndex;
    
}
@end
