//
//  HourLabel.m
//  Tasks
//
//  Created by Desislava  Nikolova on 9/30/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import "HourLabel.h"
#import "AppData.h"

@implementation HourLabel

@synthesize hour, minute;
@synthesize source;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImage *bg = [UIImage imageNamed:@"largeButtonNewEntry.png"];
        //[self setBackgroundImage:bg forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont systemFontOfSize:15];
        self.titleLabel.textColor = [UIColor darkGrayColor];
        UIImage *taskTopic = [UIImage imageNamed:@"alarm.png"];
        
        _priority = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"redBall"]];
        _priority.frame = CGRectMake(22.0, 10.0, bg.size.height - 20.0, bg.size.height - 20.0);
        _priority.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_priority];
        _priority.hidden = YES;
        
        taskTopicImg = [[UIImageView alloc] initWithImage:taskTopic];
        taskTopicImg.frame = CGRectMake(25.0, 14.0, bg.size.height - 27.0, bg.size.height - 27.0);
        taskTopicImg.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:taskTopicImg];
        
        
        _delete = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect del_frame = CGRectMake(280, 70.0, bg.size.height - 27.0, bg.size.height - 27.0);
        _delete.frame = del_frame;
        [_delete setImage:[UIImage imageNamed:@"waste_basket.png"] forState:UIControlStateNormal];
        [_delete addTarget:self action:@selector(deleteTask) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_delete];
        _delete.hidden = YES;
        
        _edit = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect edit_frame = CGRectMake(278, 14.0, bg.size.height - 24.0, bg.size.height - 24.0);
        _edit.frame = edit_frame;
        [_edit setImage:[UIImage imageNamed:@"edit-icon.png"] forState:UIControlStateNormal];
        [_edit addTarget:self action:@selector(editTask) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_edit];
        _edit.hidden = YES;
        
        //title
        _title = [[UILabel alloc] initWithFrame:CGRectMake(65.0, 46.0, self.frame.size.width - 125, 35)];
        _title.backgroundColor = [UIColor clearColor];
        _title.textColor = [UIColor whiteColor];
        if([AppData instance].themeColor > 2)
            _title.textColor = [UIColor blackColor];
        _title.font = [UIFont boldSystemFontOfSize:18];
        _title.numberOfLines = 1;
        [self addSubview:_title];
        _title.hidden = YES;
        
        _duration = [[TaskLabel alloc] initWithFrame:CGRectMake(65.0, 10.0, 160.0, 35)];
        _duration.backgroundColor = [UIColor clearColor];
        _duration.textColor = [UIColor whiteColor];
        if([AppData instance].themeColor > 2)
            _duration.textColor = [UIColor blackColor];
        _duration.font = [UIFont systemFontOfSize:14];
        _duration.numberOfLines = 1;
        _duration.textAlignment = UITextAlignmentCenter;
        [_duration setNeedsDisplay];
        [self addSubview:_duration];
        _duration.hidden = YES;
        
        _status = [[TaskLabel alloc] initWithFrame:CGRectMake(65.0, 73.0, 190.0, 25)];
        _status.drawsBorder = NO;
        _status.textAlignmentCenter = NO;
        _status.fontSize = 13;
        _status.drawingTextColor = [UIColor whiteColor];
        if([AppData instance].themeColor > 2)
            _status.drawingTextColor = [UIColor blackColor];
        _status.textColor = [UIColor blackColor];
        _status.numberOfLines = 1;
        _status.textAlignment = UITextAlignmentCenter;
        [_status setNeedsDisplay];
        [self addSubview:_status];
        _status.hidden = YES;
        
        //description
        _description.hidden = YES;
        
    }
    return self;
}

/* Task is set up - config the button */

-(void)config
{
    int pos = (source.cateory < [AppData instance].topicsArray.count) ? source.cateory : 0;
    taskTopicImg.image = [UIImage imageNamed:[[AppData instance].topicsArray objectAtIndex:pos]];
    NSLog(@"task topic %d", source.cateory);
    [taskTopicImg setNeedsDisplay];
    CGRect f = CGRectMake(25.0, 49.0, taskTopicImg.frame.size.width, taskTopicImg.frame.size.height);
    taskTopicImg.frame = f;
    
    [self setTitle:@"" forState:UIControlStateNormal];
    
    _title.text = [NSString stringWithFormat:@"%@ : %@",[[AppData instance].topicsNames objectAtIndex:pos],self.source.title] ;
   // NSString *start_a = (source.startHour > 12) ? @"pm" : @"am";
   // NSString *end_a = (source.endHour > 12) ? @"pm" : @"am";
    _duration.drawingTextString = [NSString stringWithFormat:@"%d:%d - %d:%d", source.startHour, source.startMinutes,source.endHour, source.endMinutes];// @"6:00 am - 7:00 am";
    if(source.startMinutes < 10 && source.endMinutes < 10)
    {
        _duration.drawingTextString = [NSString stringWithFormat:@"%d:0%d - %d:0%d", source.startHour, source.startMinutes,source.endHour, source.endMinutes];
    }else if(source.startMinutes < 10 && source.endMinutes > 10)
    {
        _duration.drawingTextString = [NSString stringWithFormat:@"%d:0%d - %d:%d", source.startHour, source.startMinutes,source.endHour, source.endMinutes];
    }else if(source.startMinutes > 10 && source.endMinutes < 10)
    {
        _duration.drawingTextString = [NSString stringWithFormat:@"%d:%d - %d:0%d", source.startHour, source.startMinutes,source.endHour, source.endMinutes];
    }
    _duration.completed = self.source.status;
    [_duration setNeedsDisplay];
    
    _status.drawingTextString = (source.status == 0) ? @"Status: Pending" : @"Status: Completed";
    [_status setNeedsDisplay];
    
    if(source.taskPriority == 2)
        _priority.image = [UIImage imageNamed:@"yellowBall"];
    if(source.taskPriority == 3)
        _priority.image = [UIImage imageNamed:@"greenBall"];
    
    _priority.hidden = NO;
    _title.hidden = NO;
    _duration.hidden = NO;
    _delete.hidden = NO;
    _status.hidden = NO;
    _edit.hidden = NO;
    
    
    //if more than 30' (even for 10' will draw the default button height) , show the task description /truncate right/
}


-(void)configAsCompleted
{
    taskTopicImg.image = [self imageWithColor:[UIColor redColor] image:[UIImage imageNamed:@"pacman.png"]] ;
    [taskTopicImg setNeedsDisplay];
    CGRect f = CGRectMake(25.0, 49.0, taskTopicImg.frame.size.width, taskTopicImg.frame.size.height);
    taskTopicImg.frame = f;
    
    [self setTitle:@"" forState:UIControlStateNormal];
    
    _title.text = self.source.title;
    //NSString *start_a = (source.startHour > 12) ? @"pm" : @"am";
    //NSString *end_a = (source.endHour > 12) ? @"pm" : @"am";
    _duration.drawingTextString = [NSString stringWithFormat:@"%d:%d - %d:%d", source.startHour, source.startMinutes,source.endHour, source.endMinutes ];// @"6:00 am - 7:00 am";
    _duration.completed = self.source.status;
    [_duration setNeedsDisplay];
    
    _status.drawingTextString = (source.status == 0) ? @"Status: Pending" : @"Status: Completed";
    [_status setNeedsDisplay];
    
    if(source.taskPriority == 2)
        _priority.image = [UIImage imageNamed:@"yellowBall"];
    if(source.taskPriority == 3)
        _priority.image = [UIImage imageNamed:@"greenBall"];
    
    _priority.hidden = NO;
    _title.hidden = NO;
    _duration.hidden = NO;
    _delete.hidden = NO;
    _status.hidden = NO;
    _edit.hidden = NO;
    
    
    //if more than 30' (even for 10' will draw the default button height) , show the task description /truncate right/
}


- (void)drawRect:(CGRect)rect
{
    
    // Drawing with a white stroke color
    CGContextRef context=UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0);
    if([AppData instance].themeColor > 2)
        CGContextSetRGBStrokeColor(context, 1.0, 1.0, 1.0, 1.0);
    if(source.status == 1)
    {
          CGContextSetFillColorWithColor(context, kTheme_Task_Done);
    }else if(source.edited && source.status == 0)
    {
        if([AppData instance].themeColor == 2)
        {
            if(source.taskPriority == 1)
                CGContextSetFillColorWithColor(context, kTheme2_ColorP1);
            if(source.taskPriority == 2)
                CGContextSetFillColorWithColor(context, kTheme2_ColorP2);
            if(source.taskPriority == 3)
                CGContextSetFillColorWithColor(context, kTheme2_ColorP3);
        }else if([AppData instance].themeColor == 1)
        {
            if(source.taskPriority == 1)
                CGContextSetFillColorWithColor(context, kTheme1_ColorP1);
            if(source.taskPriority == 2)
                CGContextSetFillColorWithColor(context, kTheme1_ColorP2);
            if(source.taskPriority == 3)
                CGContextSetFillColorWithColor(context, kTheme1_ColorP3);
        }else if([AppData instance].themeColor == 3)
        {
            if(source.taskPriority == 1)
                CGContextSetFillColorWithColor(context, kTheme3_ColorP1);
            if(source.taskPriority == 2)
                CGContextSetFillColorWithColor(context, kTheme3_ColorP2);
            if(source.taskPriority == 3)
                CGContextSetFillColorWithColor(context, kTheme3_ColorP3);
        }else if([AppData instance].themeColor == 4)
        {
            if(source.taskPriority == 1)
                CGContextSetFillColorWithColor(context, kTheme4_ColorP1);
            if(source.taskPriority == 2)
                CGContextSetFillColorWithColor(context, kTheme4_ColorP2);
            if(source.taskPriority == 3)
                CGContextSetFillColorWithColor(context, kTheme4_ColorP3);
        }
    }else{
          CGContextSetRGBFillColor(context, 207.0/255.0, 145.0/255.0, 107.0/255.0, 0.11);
    }
  
    
    //[self drawBevelWithRect:self.frame inContext:context];
    //UIColor *color = [UIColor colorWithRed:2.0/255.0 green:67.0/255.0 blue:143.0/255.0 alpha:1.0];
    //CGContextSetShadowWithColor (context,CGSizeMake(self.frame.size.width, self.frame.size.height / 3),3,color.CGColor);
    // If you were making this as a routine, you would probably accept a rectangle
    // that defines its bounds, and a radius reflecting the "rounded-ness" of the rectangle.
    CGRect rrect = CGRectMake(3.0, 3.0, self.frame.size.width - 7.0, self.frame.size.height - 6.0);
    CGFloat radius = 6.0;
    // NOTE: At this point you may want to verify that your radius is no more than half
    // the width and height of your rectangle, as this technique degenerates for those cases.
    
    // In order to draw a rounded rectangle, we will take advantage of the fact that
    // CGContextAddArcToPoint will draw straight lines past the start and end of the arc
    // in order to create the path from the current position and the destination position.
    
    // In order to create the 4 arcs correctly, we need to know the min, mid and max positions
    // on the x and y lengths of the given rectangle.
    CGFloat minx = CGRectGetMinX(rrect), midx = CGRectGetMidX(rrect), maxx = CGRectGetMaxX(rrect);
    CGFloat miny = CGRectGetMinY(rrect), midy = CGRectGetMidY(rrect), maxy = CGRectGetMaxY(rrect);
    
    // Next, we will go around the rectangle in the order given by the figure below.
    //       minx    midx    maxx
    // miny    2       3       4
    // midy   1 9              5
    // maxy    8       7       6
    // Which gives us a coincident start and end point, which is incidental to this technique, but still doesn't
    // form a closed path, so we still need to close the path to connect the ends correctly.
    // Thus we start by moving to point 1, then adding arcs through each pair of points that follows.
    // You could use a similar tecgnique to create any shape with rounded corners.
    
    // Start at 1
    CGContextMoveToPoint(context, minx, midy);
    // Add an arc through 2 to 3
    CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
    // Add an arc through 4 to 5
    CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);
    // Add an arc through 6 to 7
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
    // Add an arc through 8 to 9
    CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);
    // Close the path 
    CGContextClosePath(context); 
    // Fill & stroke the path 
    CGContextDrawPath(context, kCGPathFillStroke);
}

-(void)drawBevelWithRect:(CGRect)rect inContext:(CGContextRef)context{
    
    float radius = 90.0;
    
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, CGRectGetMinX(rect) + radius, CGRectGetMinY(rect));
    CGContextAddArc(context, CGRectGetMaxX(rect) - radius, CGRectGetMinY(rect) + radius, radius, 3 * M_PI / 2, 0, 0);
    
    CGFloat yCoord=CGRectGetMinY(rect)+radius;
    if (0.2*CGRectGetMaxY(rect)>10.0f) {
        yCoord=0.2*CGRectGetMaxY(rect);
    }
    CGContextAddLineToPoint(context, CGRectGetMaxX(rect), yCoord);
    
    CGContextAddQuadCurveToPoint(context,CGRectGetMaxX(rect)/2.0f,0.3*CGRectGetMaxY(rect),CGRectGetMinX(rect), yCoord);
    CGContextAddArc(context, CGRectGetMinX(rect) + radius, CGRectGetMinY(rect) + radius, radius, M_PI, 3 * M_PI / 2, 0);
    
    CGContextClosePath(context);
    
}

-(void)deleteTask
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteTaskEntry" object:self.source];
}

-(void)editTask
{
   [[NSNotificationCenter defaultCenter] postNotificationName:@"EditTaskEntry" object:self.source];
}

-(UIImage*)imageWithColor:(UIColor*)color image:(UIImage*)image
{
    //UIImage *image = [UIImage imageNamed:@"triangle.png"];
    
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClipToMask(context, rect, image.CGImage);
    CGSize shadowSize = CGSizeMake(4, 4);
    CGContextSetShadowWithColor(context, shadowSize, 0,
                                color.CGColor);
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage *flippedImage = [UIImage imageWithCGImage:img.CGImage
                                                scale:1.0 orientation: UIImageOrientationDownMirrored];
    
    return flippedImage;
}


@end
