//
//  Task.h
//  Tasks
//
//  Created by Desislava  Nikolova on 9/16/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject

enum TaskPriority {
  low = 101,
  medium = 102,
  high = 103
};

@property(nonatomic,retain)UIButton *view;
@property(nonatomic,retain)NSString *title;
@property(nonatomic,retain)NSString *description;
@property(nonatomic,assign)int cateory;
@property(nonatomic,retain)NSString *date;//MM:DD:YY (id stamp)
@property(nonatomic,assign)int day;
@property(nonatomic,assign)int month;
@property(nonatomic,assign)int year;
@property(nonatomic, assign)int taskID, status;

@property(nonatomic,assign)int startHour;
@property(nonatomic,assign)int startMinutes;

@property(nonatomic,assign)int endHour;
@property(nonatomic,assign)int endMinutes;

@property(nonatomic,assign)int isAM; //am vs pm

@property(nonatomic, assign)int taskPriority;

//utils:
@property(nonatomic,assign)BOOL edited;//not empty
@property(nonatomic,assign)BOOL displayed;
@property(nonatomic,retain)NSString *buttonTitle;

-(Task*)copyTaskObject;
-(void)reset;

@end
