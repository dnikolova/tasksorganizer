//
//  TaskView.h
//  Tasks
//
//  Created by Desislava  Nikolova on 9/22/12.
//  Copyright (c) 2012 DesiNikolova. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TaskView : UIView

@property(nonatomic, retain) IBOutlet UILabel *taskTitle;

@end
